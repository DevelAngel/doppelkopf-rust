use std::collections::VecDeque;
use tui::{
    buffer::Buffer,
    layout::{Corner, Rect},
    style::Style,
    text::Text,
    widgets::{Block, StatefulWidget, Widget},
};
use unicode_width::UnicodeWidthStr;

#[derive(Debug, Clone, Default)]
pub struct ListState {
    offset: usize,
    selected: Option<usize>,
}

impl ListState {
    pub fn selected(&self) -> Option<usize> {
        self.selected
    }

    pub fn select(&mut self, index: Option<usize>) {
        self.selected = index;
        if index.is_none() {
            self.offset = 0;
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct ListItem<'a> {
    content: Text<'a>,
    style: Style,
}

impl<'a> ListItem<'a> {
    pub fn new<T>(content: T) -> ListItem<'a>
    where
        T: Into<Text<'a>>,
    {
        ListItem {
            content: content.into(),
            style: Style::default(),
        }
    }

    #[allow(dead_code)]
    pub fn style(mut self, style: Style) -> ListItem<'a> {
        self.style = style;
        self
    }

    pub fn height(&self) -> usize {
        self.content.height()
    }

    pub fn width(&self) -> usize {
        self.content.width()
    }
}

/// A widget to display several items among which one can be selected (optional)
///
/// # Examples
///
/// ```
/// # use crate::widgets::{List, ListItem};
/// # use tui::widgets::{Block, Borders};
/// # use tui::style::{Style, Color, Modifier};
/// let items = [ListItem::new("Item 1"), ListItem::new("Item 2"), ListItem::new("Item 3")];
/// List::new(items)
///     .block(Block::default().title("List").borders(Borders::ALL))
///     .style(Style::default().fg(Color::White))
///     .highlight_style(Style::default().add_modifier(Modifier::ITALIC))
///     .highlight_symbol(">>");
/// ```
#[derive(Debug, Clone)]
pub struct List<'a> {
    block: Option<Block<'a>>,
    items: Vec<ListItem<'a>>,
    /// Style used as a base style for the widget
    style: Style,
    start_corner: Corner,
    /// Style used to render selected item
    highlight_style: Style,
    /// Symbol in front of the selected item (Shift all items to the right)
    highlight_symbol: Option<&'a str>,
    /// Whether to repeat the highlight symbol for each line of the selected item
    repeat_highlight_symbol: bool,
    /// Separation between columns,
    /// defaults to 1 if a highlight symbol is shown else defaults to 2
    sep_width: Option<u16>,
    /// override maximum height (Some(0) will be handled as None)
    max_height: Option<u16>,
}

impl<'a> List<'a> {
    pub fn new<T>(items: T) -> List<'a>
    where
        T: Into<Vec<ListItem<'a>>>,
    {
        List {
            block: None,
            style: Style::default(),
            items: items.into(),
            start_corner: Corner::TopLeft,
            highlight_style: Style::default(),
            highlight_symbol: None,
            repeat_highlight_symbol: false,
            sep_width: None,
            max_height: None,
        }
    }

    pub fn block(mut self, block: Block<'a>) -> List<'a> {
        self.block = Some(block);
        self
    }

    pub fn style(mut self, style: Style) -> List<'a> {
        self.style = style;
        self
    }

    #[allow(dead_code)]
    pub fn highlight_symbol(mut self, highlight_symbol: &'a str) -> List<'a> {
        self.highlight_symbol = Some(highlight_symbol);
        self
    }

    pub fn highlight_style(mut self, style: Style) -> List<'a> {
        self.highlight_style = style;
        self
    }

    #[allow(dead_code)]
    pub fn repeat_highlight_symbol(mut self, repeat: bool) -> List<'a> {
        self.repeat_highlight_symbol = repeat;
        self
    }

    #[allow(dead_code)]
    pub fn start_corner(mut self, corner: Corner) -> List<'a> {
        self.start_corner = corner;
        self
    }

    pub fn sep_width(mut self, sep_width: Option<u16>) -> List<'a> {
        self.sep_width = sep_width;
        self
    }

    pub fn max_height(mut self, max_height: Option<u16>) -> List<'a> {
        self.max_height = match max_height {
            None | Some(0) => None,
            Some(max_height) => Some(max_height),
        };
        self
    }

    fn get_sep_width(&self, selected: &Option<usize>) -> u16 {
        if let Some(sep_width) = self.sep_width {
            sep_width
        } else if selected.is_some() {
            1
        } else {
            2
        }
    }

    fn get_symbol_width(&self, selected: &Option<usize>) -> usize {
        match (selected, self.highlight_symbol) {
            (None, _) | (_, None) => 0,
            (Some(_), Some(symbol)) => symbol.width(),
        }
    }

    fn get_items_width(&self) -> usize {
        self.items
            .iter()
            .fold(0, |max_width, item| max_width.max(item.width()))
    }

    fn get_items_bounds(
        &self,
        selected: Option<usize>,
        offset: usize,
        max_item_width: usize,
        max_width: usize,
        max_height: usize,
    ) -> VecDeque<usize> {
        let last_item_index = self.items.len().saturating_sub(1);
        let sep_width = self.get_sep_width(&selected) as usize;
        let symbol_width = self.get_symbol_width(&selected);
        let offset = offset.min(last_item_index);

        // worst case: each item has its own column
        let mut bounds = VecDeque::with_capacity(self.items.len().saturating_add(1));
        let mut end = offset;
        {
            let mut current_width = 0;
            while end < self.items.len()
                && current_width + max_item_width + symbol_width <= max_width
            {
                let start = end;
                let mut height = 0;
                for item in self.items.iter().skip(start) {
                    if height + item.height() > max_height {
                        break;
                    }
                    height += item.height();
                    end = end.saturating_add(1);
                }
                bounds.push_back(start);
                current_width += max_item_width + symbol_width + sep_width;
            }
            bounds.push_back(end);
        }

        if let Some(selected) = selected {
            let selected = selected.min(last_item_index);

            while selected >= end {
                bounds.pop_front();
                let start = end;
                let mut height = 0;
                for item in self.items.iter().skip(start) {
                    if height + item.height() > max_height {
                        break;
                    }
                    height += item.height();
                    end = end.saturating_add(1);
                }
                bounds.push_back(end);
            }

            let &(mut start) = bounds.front().unwrap_or(&0);
            while selected < start {
                bounds.pop_back();
                let mut height = 0;
                for item in self.items.iter().take(start).rev() {
                    if height + item.height() > max_height {
                        break;
                    }
                    height += item.height();
                    start = start.saturating_sub(1);
                }
                bounds.push_front(start);
            }
        }

        bounds
    }
}

impl<'a> StatefulWidget for List<'a> {
    type State = ListState;

    fn render(mut self, area: Rect, buf: &mut Buffer, state: &mut Self::State) {
        buf.set_style(area, self.style);
        let list_area = match self.block.take() {
            Some(b) => {
                let inner_area = b.inner(area);
                b.render(area, buf);
                inner_area
            }
            None => area,
        };

        if list_area.width < 1 || list_area.height < 1 {
            return;
        }

        if self.items.is_empty() {
            return;
        }
        let list_height = match self.max_height {
            None | Some(0) => list_area.height,
            Some(max_height) => list_area.height.min(max_height),
        } as usize;
        let list_width = self.get_items_width();
        let sep_width = self.get_sep_width(&state.selected) as usize;

        let has_selection = state.selected.is_some();
        let highlight_symbol = self.highlight_symbol.unwrap_or("");
        let blank_symbol = " ".repeat(highlight_symbol.width());
        let symbol_width = self.get_symbol_width(&state.selected);

        let mut current_width = 0u16;
        let bounds = self.get_items_bounds(
            state.selected,
            state.offset,
            list_width,
            list_area.width as usize,
            list_height,
        );
        state.offset = *bounds.front().unwrap_or(&0);
        let mut bounds = bounds.iter().peekable();
        while let Some(&start) = bounds.next() {
            if let Some(&end) = bounds.peek() {
                let mut current_height = 0;
                for (i, item) in self
                    .items
                    .iter_mut()
                    .enumerate()
                    .skip(start)
                    .take(end - start)
                {
                    let (x, y) = match self.start_corner {
                        Corner::BottomLeft => {
                            current_height += item.height() as u16;
                            (
                                list_area.left() + current_width,
                                list_area.bottom() - current_height,
                            )
                        }
                        _ => {
                            let pos = (
                                list_area.left() + current_width,
                                list_area.top() + current_height,
                            );
                            current_height += item.height() as u16;
                            pos
                        }
                    };

                    let area = Rect {
                        x,
                        y,
                        width: list_width as u16,
                        height: item.height() as u16,
                    };
                    let item_style = self.style.patch(item.style);
                    buf.set_style(area, item_style);

                    let is_selected = state.selected.map(|s| s == i).unwrap_or(false);
                    for (j, line) in item.content.lines.iter().enumerate() {
                        // if the item is selected, we need to display the hightlight symbol:
                        // - either for the first line of the item only,
                        // - or for each line of the item if the appropriate option is set
                        let symbol = if is_selected && (j == 0 || self.repeat_highlight_symbol) {
                            highlight_symbol
                        } else {
                            &blank_symbol
                        };
                        let elem_x = if has_selection {
                            let (elem_x, _) = buf.set_stringn(
                                x,
                                y + j as u16,
                                symbol,
                                list_width + symbol_width,
                                item_style,
                            );
                            elem_x
                        } else {
                            x
                        };
                        buf.set_spans(
                            elem_x,
                            y + j as u16,
                            line,
                            (list_width + symbol_width) as u16,
                        );
                    }
                    if is_selected {
                        buf.set_style(area, self.highlight_style);
                    }
                }
                current_width += (list_width + symbol_width + sep_width) as u16;
            }
        }
    }
}

impl<'a> Widget for List<'a> {
    fn render(self, area: Rect, buf: &mut Buffer) {
        let mut state = ListState::default();
        StatefulWidget::render(self, area, buf, &mut state);
    }
}

#[cfg(test)]
mod test_sep_width {
    use super::*;

    #[test]
    fn none_nonselected() {
        let state = ListState::default();
        assert_eq!(state.selected, None);

        let list = List::new(vec![ListItem::new("a")]);
        assert_eq!(list.sep_width, None);

        assert_eq!(list.get_sep_width(&state.selected), 2);
    }

    #[test]
    fn none_selected() {
        let mut state = ListState::default();
        state.select(Some(0));
        assert_eq!(state.selected, Some(0));

        let list = List::new(vec![ListItem::new("a")]);
        assert_eq!(list.sep_width, None);

        assert_eq!(list.get_sep_width(&state.selected), 1);
    }

    #[test]
    fn some_nonselected() {
        let state = ListState::default();
        assert_eq!(state.selected, None);

        for i in 0..10 {
            let list = List::new(vec![ListItem::new("a")]).sep_width(Some(i));
            assert_eq!(list.sep_width, Some(i));

            assert_eq!(list.get_sep_width(&state.selected), i);
        }
    }

    #[test]
    fn some_selected() {
        let mut state = ListState::default();
        state.select(Some(0));
        assert_eq!(state.selected, Some(0));

        for i in 0..10 {
            let list = List::new(vec![ListItem::new("a")]).sep_width(Some(i));
            assert_eq!(list.sep_width, Some(i));

            assert_eq!(list.get_sep_width(&state.selected), i);
        }
    }
} // mod test_sep_width

#[cfg(test)]
mod test_items_width {
    use super::*;

    #[test]
    fn all_equal_width() {
        let list = List::new(vec![
            ListItem::new("aaa"),
            ListItem::new("bbb"),
            ListItem::new("ccc"),
        ]);

        assert_eq!(list.get_items_width(), 3);
    }

    #[test]
    fn different_width_test1() {
        let list = List::new(vec![
            ListItem::new("a"),
            ListItem::new("bb"),
            ListItem::new("ccc"),
        ]);

        assert_eq!(list.get_items_width(), 3);
    }

    #[test]
    fn different_width_test2() {
        let list = List::new(vec![
            ListItem::new("aaaa"),
            ListItem::new("bb"),
            ListItem::new("c"),
        ]);

        assert_eq!(list.get_items_width(), 4);
    }

    #[test]
    fn different_width_test3() {
        let list = List::new(vec![
            ListItem::new("aaa"),
            ListItem::new("bbbbb"),
            ListItem::new("c"),
        ]);

        assert_eq!(list.get_items_width(), 5);
    }
} // mod test_items_width

#[cfg(test)]
mod test_items_bounds {
    mod height_one_sep_zero_symbol_none {
        use super::super::*;

        #[test]
        fn all_fit() {
            let list = List::new(vec![
                ListItem::new("aa"),
                ListItem::new("bb"),
                ListItem::new("cc"),
            ])
            .sep_width(Some(0));
            assert_eq!(list.items.len(), 3);
            assert_eq!(list.sep_width, Some(0));

            let max_item_width = list.get_items_width();
            assert_eq!(max_item_width, 2);

            // example rendering:
            // |------|
            // |aabbcc|
            // |------|
            let max_width = 6;
            let selected = None;
            let offset = 0;
            let max_height = 1;
            assert_eq!(
                list.get_items_bounds(selected, offset, max_item_width, max_width, max_height),
                vec![0, 1, 2, 3]
            );
        }

        #[test]
        fn last_not_fit() {
            let list = List::new(vec![
                ListItem::new("aa"),
                ListItem::new("bb"),
                ListItem::new("cc"),
            ])
            .sep_width(Some(0));
            assert_eq!(list.items.len(), 3);
            assert_eq!(list.sep_width, Some(0));

            let max_item_width = list.get_items_width();
            assert_eq!(max_item_width, 2);

            // example rendering:
            // |----|
            // |aabb|
            // |----|
            // [0..2]
            let max_width = 4;
            let selected = None;
            let offset = 0;
            let max_height = 1;
            assert_eq!(
                list.get_items_bounds(selected, offset, max_item_width, max_width, max_height),
                vec![0, 1, 2]
            );
        }

        #[test]
        fn last_not_fit_completely() {
            let list = List::new(vec![
                ListItem::new("aa"),
                ListItem::new("bb"),
                ListItem::new("cc"),
            ])
            .sep_width(Some(0));
            assert_eq!(list.items.len(), 3);
            assert_eq!(list.sep_width, Some(0));

            let max_item_width = list.get_items_width();
            assert_eq!(max_item_width, 2);

            // example rendering:
            // |-----| wrong: |-----|
            // |aabb |        |aabbcc
            // |-----|        |-----|
            // [0..2]         [0..3]
            let max_width = 5;
            let selected = None;
            let offset = 0;
            let max_height = 1;
            assert_eq!(
                list.get_items_bounds(selected, offset, max_item_width, max_width, max_height),
                vec![0, 1, 2]
            );
        }

        #[test]
        fn first_not_fit() {
            let list = List::new(vec![
                ListItem::new("aa"),
                ListItem::new("bb"),
                ListItem::new("cc"),
            ])
            .sep_width(Some(0));
            assert_eq!(list.items.len(), 3);
            assert_eq!(list.sep_width, Some(0));

            let max_item_width = list.get_items_width();
            assert_eq!(max_item_width, 2);

            // example rendering:
            // |----|
            // |bbcc|
            // |----|
            // [1..3]
            let max_width = 4;
            let selected = Some(2); // index
            let offset = 0;
            let max_height = 1;
            assert_eq!(
                list.get_items_bounds(selected, offset, max_item_width, max_width, max_height),
                vec![1, 2, 3]
            );
        }

        #[test]
        fn first_not_fit_completely() {
            let list = List::new(vec![
                ListItem::new("aa"),
                ListItem::new("bb"),
                ListItem::new("cc"),
            ])
            .sep_width(Some(0));
            assert_eq!(list.items.len(), 3);
            assert_eq!(list.sep_width, Some(0));

            let max_item_width = list.get_items_width();
            assert_eq!(max_item_width, 2);

            // example rendering:
            // |-----| wrong: |-----|
            // | bbcc|        aabbcc|
            // |-----|        |-----|
            // [1..3]         [0..3]
            let max_width = 5;
            let selected = Some(2); // index
            let offset = 0;
            let max_height = 1;
            assert_eq!(
                list.get_items_bounds(selected, offset, max_item_width, max_width, max_height),
                vec![1, 2, 3]
            );
        }

        #[test]
        fn firstlast_not_fit() {
            let list = List::new(vec![
                ListItem::new("aa"),
                ListItem::new("bb"),
                ListItem::new("cc"),
                ListItem::new("dd"),
                ListItem::new("ee"),
            ])
            .sep_width(Some(0));
            assert_eq!(list.items.len(), 5);
            assert_eq!(list.sep_width, Some(0));

            let max_item_width = list.get_items_width();
            assert_eq!(max_item_width, 2);

            // example rendering:
            // |------|
            // |bbccdd|
            // |------|
            // [1..4]
            let max_width = 6;
            let selected = Some(3);
            let offset = 0;
            let max_height = 1;
            assert_eq!(
                list.get_items_bounds(selected, offset, max_item_width, max_width, max_height),
                vec![1, 2, 3, 4]
            );
        }

        #[test]
        fn firstlast_not_fit_completely() {
            let list = List::new(vec![
                ListItem::new("aa"),
                ListItem::new("bb"),
                ListItem::new("cc"),
                ListItem::new("dd"),
                ListItem::new("ee"),
            ])
            .sep_width(Some(0));
            assert_eq!(list.items.len(), 5);
            assert_eq!(list.sep_width, Some(0));

            let max_item_width = list.get_items_width();
            assert_eq!(max_item_width, 2);

            // example rendering:
            // |-------| wrong: |-------|
            // |bbccdd |        |bbccddee
            // |-------|        |-------|
            // [1..4]           [1..5]
            let max_width = 7;
            let selected = Some(3);
            let offset = 0;
            let max_height = 1;
            assert_eq!(
                list.get_items_bounds(selected, offset, max_item_width, max_width, max_height),
                vec![1, 2, 3, 4]
            );
        }
    }

    mod height_one_sep_one_symbol_none {
        use super::super::*;

        #[test]
        fn all_fit() {
            let list = List::new(vec![
                ListItem::new("aa"),
                ListItem::new("bb"),
                ListItem::new("cc"),
            ])
            .sep_width(Some(1));
            assert_eq!(list.items.len(), 3);
            assert_eq!(list.sep_width, Some(1));

            let max_item_width = list.get_items_width();
            assert_eq!(max_item_width, 2);

            // example rendering:
            // |--------|
            // |aa bb cc|
            // |--------|
            let max_width = 8;
            let selected = None;
            let offset = 0;
            let max_height = 1;
            assert_eq!(
                list.get_items_bounds(selected, offset, max_item_width, max_width, max_height),
                vec![0, 1, 2, 3]
            );
        }

        #[test]
        fn last_not_fit() {
            let list = List::new(vec![
                ListItem::new("aa"),
                ListItem::new("bb"),
                ListItem::new("cc"),
            ])
            .sep_width(Some(1));
            assert_eq!(list.items.len(), 3);
            assert_eq!(list.sep_width, Some(1));

            let max_item_width = list.get_items_width();
            assert_eq!(max_item_width, 2);

            // example rendering:
            // |-----|
            // |aa bb|
            // |-----|
            // [0..2]
            let max_width = 5;
            let selected = None;
            let offset = 0;
            let max_height = 1;
            assert_eq!(
                list.get_items_bounds(selected, offset, max_item_width, max_width, max_height),
                vec![0, 1, 2]
            );
        }

        #[test]
        fn last_not_fit_completely() {
            let list = List::new(vec![
                ListItem::new("aa"),
                ListItem::new("bb"),
                ListItem::new("cc"),
            ])
            .sep_width(Some(1));
            assert_eq!(list.items.len(), 3);
            assert_eq!(list.sep_width, Some(1));

            let max_item_width = list.get_items_width();
            assert_eq!(max_item_width, 2);

            // example rendering:
            // |-------| wrong: |-------|
            // |aa bb  |        |aa bb cc
            // |-------|        |-------|
            // [0..2]           [0..3]
            let max_width = 7;
            let selected = None;
            let offset = 0;
            let max_height = 1;
            assert_eq!(
                list.get_items_bounds(selected, offset, max_item_width, max_width, max_height),
                vec![0, 1, 2]
            );
        }

        #[test]
        fn first_not_fit() {
            let list = List::new(vec![
                ListItem::new("aa"),
                ListItem::new("bb"),
                ListItem::new("cc"),
            ])
            .sep_width(Some(1));
            assert_eq!(list.items.len(), 3);
            assert_eq!(list.sep_width, Some(1));

            let max_item_width = list.get_items_width();
            assert_eq!(max_item_width, 2);

            // example rendering:
            // |-----|
            // |bb cc|
            // |-----|
            // [1..3]
            let max_width = 5;
            let selected = Some(2); // index
            let offset = 0;
            let max_height = 1;
            assert_eq!(
                list.get_items_bounds(selected, offset, max_item_width, max_width, max_height),
                vec![1, 2, 3]
            );
        }

        #[test]
        fn first_not_fit_completely() {
            let list = List::new(vec![
                ListItem::new("aa"),
                ListItem::new("bb"),
                ListItem::new("cc"),
            ])
            .sep_width(Some(1));
            assert_eq!(list.items.len(), 3);
            assert_eq!(list.sep_width, Some(1));

            let max_item_width = list.get_items_width();
            assert_eq!(max_item_width, 2);

            // example rendering:
            // |------| wrong:  |------|
            // | bb cc|        aa bb cc|
            // |------|         |------|
            // [1..3]           [0..3]
            let max_width = 6;
            let selected = Some(2); // index
            let offset = 0;
            let max_height = 1;
            assert_eq!(
                list.get_items_bounds(selected, offset, max_item_width, max_width, max_height),
                vec![1, 2, 3]
            );
        }

        #[test]
        fn firstlast_not_fit() {
            let list = List::new(vec![
                ListItem::new("aa"),
                ListItem::new("bb"),
                ListItem::new("cc"),
                ListItem::new("dd"),
                ListItem::new("ee"),
            ])
            .sep_width(Some(1));
            assert_eq!(list.items.len(), 5);
            assert_eq!(list.sep_width, Some(1));

            let max_item_width = list.get_items_width();
            assert_eq!(max_item_width, 2);

            // example rendering:
            // |--------|
            // |bb cc dd|
            // |--------|
            // [1..4]
            let max_width = 8;
            let selected = Some(3);
            let offset = 0;
            let max_height = 1;
            assert_eq!(
                list.get_items_bounds(selected, offset, max_item_width, max_width, max_height),
                vec![1, 2, 3, 4]
            );
        }

        #[test]
        fn firstlast_not_fit_completely() {
            let list = List::new(vec![
                ListItem::new("aa"),
                ListItem::new("bb"),
                ListItem::new("cc"),
                ListItem::new("dd"),
                ListItem::new("ee"),
            ])
            .sep_width(Some(1));
            assert_eq!(list.items.len(), 5);
            assert_eq!(list.sep_width, Some(1));

            let max_item_width = list.get_items_width();
            assert_eq!(max_item_width, 2);

            // example rendering:
            // |----------| wrong: |----------|
            // |bb cc dd  |        |bb cc dd ee
            // |----------|        |----------|
            // [1..4]              [1..5]
            let max_width = 10;
            let selected = Some(3);
            let offset = 0;
            let max_height = 1;
            assert_eq!(
                list.get_items_bounds(selected, offset, max_item_width, max_width, max_height),
                vec![1, 2, 3, 4]
            );
        }
    }

    mod height_one_sep_zero_symbol_one {
        use super::super::*;

        #[test]
        fn all_fit() {
            let list = List::new(vec![
                ListItem::new("aa"),
                ListItem::new("bb"),
                ListItem::new("cc"),
            ])
            .sep_width(Some(0))
            .highlight_symbol(">");
            assert_eq!(list.items.len(), 3);
            assert_eq!(list.sep_width, Some(0));
            assert_eq!(list.highlight_symbol.unwrap().width(), 1);

            let max_item_width = list.get_items_width();
            assert_eq!(max_item_width, 2);

            // example rendering:
            // |---------|
            // | aa>bb cc|
            // |---------|
            let max_width = 9;
            let selected = Some(1);
            let offset = 0;
            let max_height = 1;
            assert_eq!(
                list.get_items_bounds(selected, offset, max_item_width, max_width, max_height),
                vec![0, 1, 2, 3]
            );
        }

        #[test]
        fn last_not_fit() {
            let list = List::new(vec![
                ListItem::new("aa"),
                ListItem::new("bb"),
                ListItem::new("cc"),
            ])
            .sep_width(Some(0))
            .highlight_symbol(">");
            assert_eq!(list.items.len(), 3);
            assert_eq!(list.sep_width, Some(0));
            assert_eq!(list.highlight_symbol.unwrap().width(), 1);

            let max_item_width = list.get_items_width();
            assert_eq!(max_item_width, 2);

            // example rendering:
            // |------|
            // | aa>bb|
            // |------|
            // [0..2]
            let max_width = 6;
            let selected = Some(1);
            let offset = 0;
            let max_height = 1;
            assert_eq!(
                list.get_items_bounds(selected, offset, max_item_width, max_width, max_height),
                vec![0, 1, 2]
            );
        }

        #[test]
        fn last_not_fit_completely() {
            let list = List::new(vec![
                ListItem::new("aa"),
                ListItem::new("bb"),
                ListItem::new("cc"),
            ])
            .sep_width(Some(0))
            .highlight_symbol(">");
            assert_eq!(list.items.len(), 3);
            assert_eq!(list.sep_width, Some(0));
            assert_eq!(list.highlight_symbol.unwrap().width(), 1);

            let max_item_width = list.get_items_width();
            assert_eq!(max_item_width, 2);

            // example rendering:
            // |--------| wrong: |--------|
            // | aa>bb  |        | aa>bb cc
            // |--------|        |--------|
            // [0..2]            [0..3]
            let max_width = 8;
            let selected = Some(1);
            let offset = 0;
            let max_height = 1;
            assert_eq!(
                list.get_items_bounds(selected, offset, max_item_width, max_width, max_height),
                vec![0, 1, 2]
            );
        }

        #[test]
        fn first_not_fit() {
            let list = List::new(vec![
                ListItem::new("aa"),
                ListItem::new("bb"),
                ListItem::new("cc"),
            ])
            .sep_width(Some(0))
            .highlight_symbol(">");
            assert_eq!(list.items.len(), 3);
            assert_eq!(list.sep_width, Some(0));
            assert_eq!(list.highlight_symbol.unwrap().width(), 1);

            let max_item_width = list.get_items_width();
            assert_eq!(max_item_width, 2);

            // example rendering:
            // |------|
            // | bb>cc|
            // |------|
            // [1..3]
            let max_width = 6;
            let selected = Some(2); // index
            let offset = 0;
            let max_height = 1;
            assert_eq!(
                list.get_items_bounds(selected, offset, max_item_width, max_width, max_height),
                vec![1, 2, 3]
            );
        }

        #[test]
        fn first_not_fit_completely() {
            let list = List::new(vec![
                ListItem::new("aa"),
                ListItem::new("bb"),
                ListItem::new("cc"),
            ])
            .sep_width(Some(0))
            .highlight_symbol(">");
            assert_eq!(list.items.len(), 3);
            assert_eq!(list.sep_width, Some(0));
            assert_eq!(list.highlight_symbol.unwrap().width(), 1);

            let max_item_width = list.get_items_width();
            assert_eq!(max_item_width, 2);

            // example rendering:
            // |--------| wrong: |--------|
            // |   bb>cc|         aa bb>cc|
            // |--------|        |--------|
            // [1..3]            [0..3]
            let max_width = 8;
            let selected = Some(2); // index
            let offset = 0;
            let max_height = 1;
            assert_eq!(
                list.get_items_bounds(selected, offset, max_item_width, max_width, max_height),
                vec![1, 2, 3]
            );
        }

        #[test]
        fn firstlast_not_fit() {
            let list = List::new(vec![
                ListItem::new("aa"),
                ListItem::new("bb"),
                ListItem::new("cc"),
                ListItem::new("dd"),
                ListItem::new("ee"),
            ])
            .sep_width(Some(0))
            .highlight_symbol(">");
            assert_eq!(list.items.len(), 5);
            assert_eq!(list.sep_width, Some(0));
            assert_eq!(list.highlight_symbol.unwrap().width(), 1);

            let max_item_width = list.get_items_width();
            assert_eq!(max_item_width, 2);

            // example rendering:
            // |---------|
            // | bb cc>dd|
            // |---------|
            // [1..4]
            let max_width = 9;
            let selected = Some(3);
            let offset = 0;
            let max_height = 1;
            assert_eq!(
                list.get_items_bounds(selected, offset, max_item_width, max_width, max_height),
                vec![1, 2, 3, 4]
            );
        }

        #[test]
        fn firstlast_not_fit_completely() {
            let list = List::new(vec![
                ListItem::new("aa"),
                ListItem::new("bb"),
                ListItem::new("cc"),
                ListItem::new("dd"),
                ListItem::new("ee"),
            ])
            .sep_width(Some(0))
            .highlight_symbol(">");
            assert_eq!(list.items.len(), 5);
            assert_eq!(list.sep_width, Some(0));
            assert_eq!(list.highlight_symbol.unwrap().width(), 1);

            let max_item_width = list.get_items_width();
            assert_eq!(max_item_width, 2);

            // example rendering:
            // |-----------| wrong: |-----------|
            // | bb cc>dd  |        | bb cc>dd ee
            // |-----------|        |-----------|
            // [1..4]               [1..5]
            let max_width = 11;
            let selected = Some(3);
            let offset = 0;
            let max_height = 1;
            assert_eq!(
                list.get_items_bounds(selected, offset, max_item_width, max_width, max_height),
                vec![1, 2, 3, 4]
            );
        }
    }

    mod height_one_sep_one_symbol_one {
        use super::super::*;

        #[test]
        fn all_fit() {
            let list = List::new(vec![
                ListItem::new("aa"),
                ListItem::new("bb"),
                ListItem::new("cc"),
            ])
            .sep_width(Some(1))
            .highlight_symbol(">");
            assert_eq!(list.items.len(), 3);
            assert_eq!(list.sep_width, Some(1));
            assert_eq!(list.highlight_symbol.unwrap().width(), 1);

            let max_item_width = list.get_items_width();
            assert_eq!(max_item_width, 2);

            // example rendering:
            // |-----------|
            // | aa >bb  cc|
            // |-----------|
            let max_width = 11;
            let selected = Some(1);
            let offset = 0;
            let max_height = 1;
            assert_eq!(
                list.get_items_bounds(selected, offset, max_item_width, max_width, max_height),
                vec![0, 1, 2, 3]
            );
        }

        #[test]
        fn last_not_fit() {
            let list = List::new(vec![
                ListItem::new("aa"),
                ListItem::new("bb"),
                ListItem::new("cc"),
            ])
            .sep_width(Some(1))
            .highlight_symbol(">");
            assert_eq!(list.items.len(), 3);
            assert_eq!(list.sep_width, Some(1));
            assert_eq!(list.highlight_symbol.unwrap().width(), 1);

            let max_item_width = list.get_items_width();
            assert_eq!(max_item_width, 2);

            // example rendering:
            // |-------|
            // | aa >bb|
            // |-------|
            // [0..2]
            let max_width = 7;
            let selected = Some(1);
            let offset = 0;
            let max_height = 1;
            assert_eq!(
                list.get_items_bounds(selected, offset, max_item_width, max_width, max_height),
                vec![0, 1, 2]
            );
        }

        #[test]
        fn last_not_fit_completely() {
            let list = List::new(vec![
                ListItem::new("aa"),
                ListItem::new("bb"),
                ListItem::new("cc"),
            ])
            .sep_width(Some(1))
            .highlight_symbol(">");
            assert_eq!(list.items.len(), 3);
            assert_eq!(list.sep_width, Some(1));
            assert_eq!(list.highlight_symbol.unwrap().width(), 1);

            let max_item_width = list.get_items_width();
            assert_eq!(max_item_width, 2);

            // example rendering:
            // |----------| wrong: |----------|
            // | aa >bb   |        | aa >bb  cc
            // |----------|        |----------|
            // [0..2]              [0..3]
            let max_width = 10;
            let selected = Some(1);
            let offset = 0;
            let max_height = 1;
            assert_eq!(
                list.get_items_bounds(selected, offset, max_item_width, max_width, max_height),
                vec![0, 1, 2]
            );
        }

        #[test]
        fn first_not_fit() {
            let list = List::new(vec![
                ListItem::new("aa"),
                ListItem::new("bb"),
                ListItem::new("cc"),
            ])
            .sep_width(Some(1))
            .highlight_symbol(">");
            assert_eq!(list.items.len(), 3);
            assert_eq!(list.sep_width, Some(1));
            assert_eq!(list.highlight_symbol.unwrap().width(), 1);

            let max_item_width = list.get_items_width();
            assert_eq!(max_item_width, 2);

            // example rendering:
            // |-------|
            // | bb >cc|
            // |-------|
            // [1..3]
            let max_width = 7;
            let selected = Some(2); // index
            let offset = 0;
            let max_height = 1;
            assert_eq!(
                list.get_items_bounds(selected, offset, max_item_width, max_width, max_height),
                vec![1, 2, 3]
            );
        }

        #[test]
        fn first_not_fit_completely() {
            let list = List::new(vec![
                ListItem::new("aa"),
                ListItem::new("bb"),
                ListItem::new("cc"),
            ])
            .sep_width(Some(1))
            .highlight_symbol(">");
            assert_eq!(list.items.len(), 3);
            assert_eq!(list.sep_width, Some(1));
            assert_eq!(list.highlight_symbol.unwrap().width(), 1);

            let max_item_width = list.get_items_width();
            assert_eq!(max_item_width, 2);

            // example rendering:
            // |---------| wrong:  |---------|
            // |   bb >cc|         aa  bb >cc|
            // |---------|         |---------|
            // [1..3]              [0..3]
            let max_width = 9;
            let selected = Some(2); // index
            let offset = 0;
            let max_height = 1;
            assert_eq!(
                list.get_items_bounds(selected, offset, max_item_width, max_width, max_height),
                vec![1, 2, 3]
            );
        }

        #[test]
        fn firstlast_not_fit() {
            let list = List::new(vec![
                ListItem::new("aa"),
                ListItem::new("bb"),
                ListItem::new("cc"),
                ListItem::new("dd"),
                ListItem::new("ee"),
            ])
            .sep_width(Some(1))
            .highlight_symbol(">");
            assert_eq!(list.items.len(), 5);
            assert_eq!(list.sep_width, Some(1));
            assert_eq!(list.highlight_symbol.unwrap().width(), 1);

            let max_item_width = list.get_items_width();
            assert_eq!(max_item_width, 2);

            // example rendering:
            // |-----------|
            // | bb  cc >dd|
            // |-----------|
            // [1..4]
            let max_width = 11;
            let selected = Some(3);
            let offset = 0;
            let max_height = 1;
            assert_eq!(
                list.get_items_bounds(selected, offset, max_item_width, max_width, max_height),
                vec![1, 2, 3, 4]
            );
        }

        #[test]
        fn firstlast_not_fit_completely() {
            let list = List::new(vec![
                ListItem::new("aa"),
                ListItem::new("bb"),
                ListItem::new("cc"),
                ListItem::new("dd"),
                ListItem::new("ee"),
            ])
            .sep_width(Some(1))
            .highlight_symbol(">");
            assert_eq!(list.items.len(), 5);
            assert_eq!(list.sep_width, Some(1));
            assert_eq!(list.highlight_symbol.unwrap().width(), 1);

            let max_item_width = list.get_items_width();
            assert_eq!(max_item_width, 2);

            // example rendering:
            // |--------------| wrong: |--------------|
            // | bb  cc >dd   |        | bb  cc >dd  ee
            // |--------------|        |--------------|
            // [1..4]                  [1..5]
            let max_width = 14;
            let selected = Some(3);
            let offset = 0;
            let max_height = 1;
            assert_eq!(
                list.get_items_bounds(selected, offset, max_item_width, max_width, max_height),
                vec![1, 2, 3, 4]
            );
        }
    }
} // mod test_items_bounds
