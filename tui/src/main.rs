mod widgets;

use doppelkopf_lib::card::Card;
use doppelkopf_lib::dealer::Dealer;
use doppelkopf_lib::gamecard::GameCard;
use doppelkopf_lib::hiddencard::HiddenCard;
use doppelkopf_lib::rank::Rank;
use doppelkopf_lib::suit::Suit;

#[allow(unused_imports)]
use doppelkopf_lib::player::{GamePlayer, KIPlayer, Player, TUIPlayer};

use std::collections::VecDeque;

use crate::widgets::{List, ListItem, ListState};
use std::io;
use tui::{
    backend::{Backend, CrosstermBackend},
    layout::{Constraint, Direction, Layout, Rect},
    style::{Color, Modifier, Style},
    widgets::{Block, Borders, Paragraph, Row, Table},
    Frame, Terminal,
};

use crossterm::{
    event::{self, DisableMouseCapture, EnableMouseCapture, Event, KeyCode},
    execute,
    terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen},
};

use unicode_width::UnicodeWidthStr;

use strum::{EnumIter, IntoEnumIterator};

use clap::Parser;

struct DisplayGameCard<'a>(&'a GameCard);
struct DisplayHiddenCard<'a>(&'a HiddenCard);

impl<'a> std::fmt::Display for DisplayHiddenCard<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let bracket_open = '\u{3014}';
        let bracket_closed = '\u{3015}';
        match self.0.card() {
            None => {
                let suit_rank = "****";
                write!(f, "{}{}{}", bracket_open, suit_rank, bracket_closed)
            }
            Some(card) => {
                write!(f, "{}{}{}", bracket_open, card, bracket_closed)
            }
        }
    }
}

impl<'a> std::fmt::Display for DisplayGameCard<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let bracket_open = '\u{3014}';
        let bracket_closed = '\u{3015}';
        let card = self.0.card();
        write!(f, "{}{}{}", bracket_open, card, bracket_closed)
    }
}

impl<'a> From<&'a GameCard> for DisplayGameCard<'a> {
    fn from(src: &'a GameCard) -> Self {
        Self(src)
    }
}

impl<'a> From<&'a HiddenCard> for DisplayHiddenCard<'a> {
    fn from(src: &'a HiddenCard) -> Self {
        Self(src)
    }
}

static GAME_CARD_WIDTH: usize = 8;

fn create_player<T>(name: &'static str, ouvert: bool) -> Box<T>
where
    T: GamePlayer,
{
    let p = T::new(name, ouvert);
    Box::new(p)
}

#[derive(Parser, Default, Debug)]
#[clap(author = "Angelos Drossos", version, about)]
/// A trick-taking card game for four players
struct Arguments {
    /// show cards of KI players
    #[clap(short, long)]
    ouvert: bool,

    /// only KI players
    #[clap(short, long)]
    ki_only: bool,
}

fn main() -> Result<(), Box<dyn std::error::Error + 'static>> {
    let args = Arguments::parse();

    // setup terminal
    enable_raw_mode()?;
    let mut stdout = std::io::stdout();
    execute!(stdout, EnterAlternateScreen, EnableMouseCapture)?;
    let backend = CrosstermBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;

    // create app and run it
    let res = run_app(&mut terminal, args);

    // restore terminal
    disable_raw_mode()?;
    execute!(
        terminal.backend_mut(),
        LeaveAlternateScreen,
        DisableMouseCapture
    )?;
    terminal.show_cursor()?;

    if let Err(err) = res {
        println!("{:?}", err)
    }
    Ok(())
}

fn exit_app() -> io::Error {
    io::Error::new(io::ErrorKind::Other, "exit by user")
}

fn run_app<B: Backend>(terminal: &mut Terminal<B>, args: Arguments) -> io::Result<()> {
    let in_idle = run_app_idle(terminal, &args)?;
    let mut in_dealing = run_app_dealing(terminal, in_idle)?;
    loop {
        let in_playing = run_app_playing_normal(terminal, in_dealing)?;
        let in_scoring = run_app_scoring(terminal, in_playing)?;
        in_dealing = run_app_dealing(terminal, in_scoring)?;
    }
}

fn run_app_idle<B: Backend>(
    _terminal: &mut Terminal<B>,
    args: &Arguments,
) -> io::Result<StateMachine<StateIdle>> {
    let sm = StateMachine::new(args.ouvert, args.ki_only);
    Ok(sm)
}

fn run_app_dealing<B: Backend, SM: Into<StateMachine<StateDealing>>>(
    terminal: &mut Terminal<B>,
    sm: SM,
) -> io::Result<StateMachine<StateDealing>> {
    let mut sm = sm.into();

    for round in 1..=3 {
        if round == 2 {
            sm.state.dealer.deal_out_four_cards(&mut sm.players);
        } else {
            sm.state.dealer.deal_out_three_cards(&mut sm.players);
        }

        // draw
        {
            let (name, hand): (Vec<&str>, Vec<Vec<HiddenCard>>) =
                sm.players.iter().map(|p| (p.name(), p.hand())).unzip();
            terminal.draw(|f| ui_dealing(f, name.as_slice(), hand.as_slice()))?;
        }

        while let Event::Key(key) = event::read()? {
            match key.code {
                KeyCode::Char('q') => return Err(exit_app()), // exit
                KeyCode::Enter => break,                      // continue
                _ => {}
            }
        }
    }

    Ok(sm)
}

fn run_app_playing_normal<B: Backend, SM: Into<StateMachine<StatePlayingNormal>>>(
    terminal: &mut Terminal<B>,
    sm: SM,
) -> io::Result<StateMachine<StatePlayingNormal>> {
    let mut sm = sm.into();
    for _round in 1..=10 {
        let mut trick_cards: Vec<GameCard> = Vec::with_capacity(4);
        let mut trick_winner: Option<(usize, Card)> = None;
        let mut player_indices: VecDeque<_> = (0..4).collect();
        if let Some(trick) = sm.state.played_tricks.last() {
            player_indices.rotate_left(trick.0);
        }

        for index in player_indices.clone().into_iter() {
            let (name, hand): (Vec<String>, Vec<Vec<HiddenCard>>) = sm
                .players
                .iter()
                .map(|p| (p.name().to_owned(), p.hand()))
                .unzip();
            let card = sm.players[index].play_card(&trick_cards, &mut |hand_indices| {
                if sm.state.player_states[index].is_some() {
                    assert_ne!(hand_indices.len(), 0, "hand indices must not be empty");
                    // check if hand_indices contains selected index
                    if let Some(state) = sm.state.player_states[index].as_mut() {
                        if let Some(selected) = state.selected().as_ref() {
                            if hand_indices.binary_search(selected).is_err() {
                                state.select(Some(*hand_indices.first().unwrap()));
                            }
                        } else {
                            state.select(Some(*hand_indices.first().unwrap()));
                        }
                        assert!(state.selected().is_some());
                    }

                    // draw
                    {
                        let list_states = &mut sm.state.player_states;
                        let table: Vec<(&usize, &GameCard)> =
                            player_indices.iter().zip(trick_cards.iter()).collect();
                        let trick_winner_index = if trick_winner.is_some() {
                            &trick_winner.as_ref().unwrap().0
                        } else {
                            &index
                        };
                        let team_re = team_re_to_array(sm.state.team_re.as_slice());
                        terminal
                            .draw(|f| {
                                ui_playing(
                                    f,
                                    name.as_slice(),
                                    hand.as_slice(),
                                    team_re,
                                    list_states,
                                    table.as_slice(),
                                    trick_winner_index,
                                    false,
                                )
                            })
                            .ok()?;
                    }

                    while let Ok(Event::Key(key)) = event::read() {
                        match key.code {
                            KeyCode::Enter => break,
                            KeyCode::Left | KeyCode::Up => {
                                let state = sm.state.player_states[index].as_mut().unwrap();
                                let selected = state.selected().unwrap();
                                if let Ok(index) = hand_indices.binary_search(&selected) {
                                    state.select(Some(hand_indices[index.saturating_sub(1)]));
                                }
                            }
                            KeyCode::Right | KeyCode::Down => {
                                let state = sm.state.player_states[index].as_mut().unwrap();
                                let selected = state.selected().unwrap();
                                if let Ok(index) = hand_indices.binary_search(&selected) {
                                    state.select(Some(
                                        hand_indices[(index + 1).min(hand_indices.len() - 1)],
                                    ));
                                }
                            }
                            KeyCode::Char('l') => {
                                // draw last trick
                                if let Some((trick_winner_index, trick_cards)) =
                                    sm.state.played_tricks.last()
                                {
                                    let list_states = &mut sm.state.player_states;
                                    let trick_cards: Vec<(&usize, &GameCard)> =
                                        trick_cards.iter().map(|(i, c)| (i, c)).collect();
                                    let team_re = team_re_to_array(sm.state.team_re.as_slice());
                                    terminal
                                        .draw(|f| {
                                            ui_playing(
                                                f,
                                                name.as_slice(),
                                                hand.as_slice(),
                                                team_re,
                                                list_states,
                                                trick_cards.as_slice(),
                                                trick_winner_index,
                                                true,
                                            )
                                        })
                                        .ok()?;

                                    while let Ok(Event::Key(key)) = event::read() {
                                        match key.code {
                                            KeyCode::Char('q') => break, // continue
                                            KeyCode::Enter => break,     // continue
                                            _ => {}
                                        }
                                    }
                                }
                            }
                            _ => {}
                        }

                        // draw
                        {
                            let list_states = &mut sm.state.player_states;
                            let table: Vec<(&usize, &GameCard)> =
                                player_indices.iter().zip(trick_cards.iter()).collect();
                            let trick_winner_index = if trick_winner.is_some() {
                                &trick_winner.as_ref().unwrap().0
                            } else {
                                &index
                            };
                            let team_re = team_re_to_array(sm.state.team_re.as_slice());
                            terminal
                                .draw(|f| {
                                    ui_playing(
                                        f,
                                        name.as_slice(),
                                        hand.as_slice(),
                                        team_re,
                                        list_states,
                                        table.as_slice(),
                                        trick_winner_index,
                                        false,
                                    )
                                })
                                .ok()?;
                        }
                    }

                    let state = sm.state.player_states[index].as_ref().unwrap();
                    state.selected()
                } else {
                    None
                }
            });
            if let Some(state) = sm.state.player_states[index].as_mut() {
                state.select(None);
            }

            let team_re = &mut sm.state.team_re;
            if team_re.len() < 2
                && card.card() == &Card::new(&Suit::Club, &Rank::Queen)
                && !team_re.contains(&index)
            {
                team_re.push(index);
            }

            trick_winner = match trick_winner {
                None => Some((index, card.card().clone())), // first card
                Some((winner_index, winner_card)) => {
                    if card.card() > &winner_card {
                        Some((index, card.card().clone())) // new winner
                    } else {
                        Some((winner_index, winner_card))
                    }
                }
            };

            trick_cards.push(card);

            // draw
            {
                let (name, hand): (Vec<&str>, Vec<Vec<HiddenCard>>) =
                    sm.players.iter().map(|p| (p.name(), p.hand())).unzip();
                let list_states = &mut sm.state.player_states;
                let table: Vec<(&usize, &GameCard)> =
                    player_indices.iter().zip(trick_cards.iter()).collect();
                let team_re = team_re_to_array(team_re.as_slice());
                terminal.draw(|f| {
                    ui_playing(
                        f,
                        name.as_slice(),
                        hand.as_slice(),
                        team_re,
                        list_states,
                        table.as_slice(),
                        &trick_winner.as_ref().unwrap().0,
                        false,
                    )
                })?;
            }

            while let Event::Key(key) = event::read()? {
                match key.code {
                    KeyCode::Char('q') => return Err(exit_app()), // exit
                    KeyCode::Enter => break,                      // continue
                    //KeyCode::Left => app.on_left(),
                    //KeyCode::Right => app.on_right(),
                    _ => {}
                }
            }
        }

        assert_eq!(player_indices.len(), 4);
        assert_eq!(trick_cards.len(), 4);
        let trick_cards: [(usize, GameCard); 4] = player_indices
            .into_iter()
            .zip(trick_cards.into_iter())
            .collect::<Vec<_>>()
            .try_into()
            .unwrap_or_else(|_| panic!("expect a vector of length 4"));
        let trick_winner = trick_winner.expect("trick_winner not set"); // remove Option
        sm.state.played_tricks.push((trick_winner.0, trick_cards));
    }

    Ok(sm)
}

fn team_value(played_tricks: &[(usize, [(usize, GameCard); 4])], team: &[usize]) -> usize {
    let value =
        played_tricks
            .iter()
            .filter(|(i, _)| team.contains(i))
            .fold(0, |acc, (_, gc_array)| {
                acc + gc_array
                    .iter()
                    .fold(0, |acc, (_, gc)| acc + gc.card().value())
            });
    value
}

fn has_doppelkopf_stich(played_tricks: &[(usize, [(usize, GameCard); 4])], team: &[usize]) -> bool {
    for (_, trick) in played_tricks.iter().filter(|(i, _)| team.contains(i)) {
        let value = trick.iter().fold(0, |acc, (_, gc)| acc + gc.card().value());
        if value >= 40 {
            return true;
        }
    }
    false
}

fn has_club_jack_made_last_trick(
    played_tricks: &[(usize, [(usize, GameCard); 4])],
    team: &[usize],
) -> bool {
    assert!(!played_tricks.is_empty());
    if let Some((winner, trick)) = played_tricks.iter().last() {
        if team.contains(winner) {
            for (_, gc) in trick.iter().filter(|(i, _)| i == winner) {
                if gc.card() == &Card::new(&Suit::Club, &Rank::Jack) {
                    return true;
                }
            }
        }
    }
    false
}

fn has_fox_catched(played_tricks: &[(usize, [(usize, GameCard); 4])], team: &[usize]) -> bool {
    let fox = Card::new(&Suit::Diamond, &Rank::Ace);
    played_tricks
        .iter()
        .filter(|(i, _)| team.contains(i))
        .any(|(_, trick)| {
            trick
                .iter()
                .any(|(i, gc)| !team.contains(i) && gc.card() == &fox)
        })
}

fn run_app_scoring<B: Backend, SM: Into<StateMachine<StateScoring>>>(
    terminal: &mut Terminal<B>,
    sm: SM,
) -> io::Result<StateMachine<StateScoring>> {
    let mut sm = sm.into();

    let team_re = sm.state.team_re.as_ref();
    let team_kontra = sm.state.team_kontra.as_ref();
    let team_re_value = team_value(sm.state.played_tricks.as_ref(), team_re);
    let team_kontra_value = 240 - team_re_value;
    assert_eq!(
        team_kontra_value,
        team_value(sm.state.played_tricks.as_ref(), team_kontra)
    );

    let team_re_won = team_re_value > 120;
    let solo = team_re.len() == 1;

    let points: Vec<(ScoringPoints, Option<bool>, Option<bool>)> = ScoringPoints::iter()
        .map(|p| match p {
            ScoringPoints::Gewonnen => {
                if team_re_won {
                    (p, Some(true), None)
                } else {
                    (p, None, Some(true))
                }
            }
            ScoringPoints::Unter90 => {
                if team_re_won {
                    (p, Some(team_kontra_value < 90), None)
                } else {
                    (p, None, Some(team_re_value < 90))
                }
            }
            ScoringPoints::Unter60 => {
                if team_re_won {
                    (p, Some(team_kontra_value < 60), None)
                } else {
                    (p, None, Some(team_re_value < 60))
                }
            }
            ScoringPoints::Unter30 => {
                if team_re_won {
                    (p, Some(team_kontra_value < 30), None)
                } else {
                    (p, None, Some(team_re_value < 30))
                }
            }
            ScoringPoints::Schwarz => {
                if team_re_won {
                    (p, Some(team_kontra_value == 0), None)
                } else {
                    (p, None, Some(team_re_value == 0))
                }
            }
            ScoringPoints::AnsageRe => (p, None, None),
            ScoringPoints::AnsageKontra => (p, None, None),
            ScoringPoints::AbsageReKeine90 => (p, None, None),
            ScoringPoints::AbsageReKeine60 => (p, None, None),
            ScoringPoints::AbsageReKeine30 => (p, None, None),
            ScoringPoints::AbsageReSchwarz => (p, None, None),
            ScoringPoints::AbsageKontraKeine90 => (p, None, None),
            ScoringPoints::AbsageKontraKeine60 => (p, None, None),
            ScoringPoints::AbsageKontraKeine30 => (p, None, None),
            ScoringPoints::AbsageKontraSchwarz => (p, None, None),
            ScoringPoints::Re120GegenKeine90 => (p, None, None),
            ScoringPoints::Re90GegenKeine60 => (p, None, None),
            ScoringPoints::Re60GegenKeine30 => (p, None, None),
            ScoringPoints::Re30GegenSchwarz => (p, None, None),
            ScoringPoints::Kontra120GegenKeine90 => (p, None, None),
            ScoringPoints::Kontra90GegenKeine60 => (p, None, None),
            ScoringPoints::Kontra60GegenKeine30 => (p, None, None),
            ScoringPoints::Kontra30GegenSchwarz => (p, None, None),
            ScoringPoints::GegenDieAlten => {
                if solo || team_re_won {
                    (p, None, None)
                } else {
                    (p, None, Some(true))
                }
            }
            ScoringPoints::FuchsGefangen => {
                if solo {
                    (p, None, None)
                } else {
                    let sp_re = has_fox_catched(sm.state.played_tricks.as_ref(), team_re);
                    let sp_kontra = has_fox_catched(sm.state.played_tricks.as_ref(), team_kontra);
                    (p, Some(sp_re), Some(sp_kontra))
                }
            }
            ScoringPoints::KarlchenMueller => {
                if solo {
                    (p, None, None)
                } else {
                    let sp_re =
                        has_club_jack_made_last_trick(sm.state.played_tricks.as_ref(), team_re);
                    let sp_kontra =
                        has_club_jack_made_last_trick(sm.state.played_tricks.as_ref(), team_kontra);
                    (p, Some(sp_re), Some(sp_kontra))
                }
            }
            ScoringPoints::DoppelkopfStich => {
                if solo {
                    (p, None, None)
                } else {
                    let sp_re = has_doppelkopf_stich(sm.state.played_tricks.as_ref(), team_re);
                    let sp_kontra =
                        has_doppelkopf_stich(sm.state.played_tricks.as_ref(), team_kontra);
                    (p, Some(sp_re), Some(sp_kontra))
                }
            }
        })
        .collect();
    let game_value_re = points.iter().fold(0, |val, item| {
        if item.1.unwrap_or(false) {
            val + 1
        } else {
            val
        }
    });
    let game_value_kontra = points.iter().fold(0, |val, item| {
        if item.2.unwrap_or(false) {
            val + 1
        } else {
            val
        }
    });
    let game_value = (game_value_re as isize - game_value_kontra as isize).abs();

    terminal.draw(|f| {
        ui_scoring_game_value(
            f,
            points.as_ref(),
            (game_value_re, game_value_kontra, game_value as usize),
        )
    })?;

    while let Event::Key(key) = event::read()? {
        match key.code {
            KeyCode::Char('q') => return Err(exit_app()), // exit
            KeyCode::Enter => break,                      // continue
            _ => {}
        }
    }

    assert_eq!(team_re.len() + team_kontra.len(), 4);
    let mut player_scoring = [0; 4];
    if solo {
        assert_eq!(team_re.len(), 1);
        assert_eq!(team_kontra.len(), 3);
        let game_value_solo = game_value * 3;
        for &i in team_re.iter() {
            player_scoring[i] = if team_re_won {
                game_value_solo
            } else {
                -game_value_solo
            };
        }
        for &i in team_kontra.iter() {
            player_scoring[i] = if team_re_won { -game_value } else { game_value };
        }
    } else {
        assert_eq!(team_kontra.len(), 2);
        for &i in team_re.iter() {
            player_scoring[i] = if team_re_won { game_value } else { -game_value };
        }
        for &i in team_kontra.iter() {
            player_scoring[i] = if team_re_won { -game_value } else { game_value };
        }
    }

    sm.scoring_table.push(ScoringRound {
        game_value: game_value as usize,
        player_scoring,
    });

    terminal.draw(|f| ui_scoring_table(f, &sm))?;

    while let Event::Key(key) = event::read()? {
        match key.code {
            KeyCode::Char('q') => return Err(exit_app()), // exit
            KeyCode::Enter => break,                      // continue
            _ => {}
        }
    }

    Ok(sm)
}

fn team_re_to_array(team_re: &[usize]) -> [bool; 4] {
    let mut array = [false; 4];
    for &i in team_re.iter() {
        assert!(i < 4);
        array[i] = true;
    }
    array
}

struct GameChunks {
    player_left: Rect,
    player_top: Rect,
    player_right: Rect,
    player_bottom: Rect,
    table: Rect,
}

fn calc_game_chunks<B: Backend>(f: &mut Frame<B>) -> GameChunks {
    /* f.size()
     * +----------------------------------------------------+
     * |                  chunks[0]                         |
     * +----------------------------------------------------+
     * |                                                    |
     * |                                                    |
     * |                                                    |
     * |                                                    |
     * |                                                    |
     * |                  chunks[1]                         |
     * |                                                    |
     * |                                                    |
     * |                                                    |
     * |                                                    |
     * |                                                    |
     * |                                                    |
     * +----------------------------------------------------+
     * |                  chunks[2]                         |
     * +----------------------------------------------------+
     */
    let chunks = Layout::default()
        .direction(Direction::Vertical)
        .constraints(
            [
                Constraint::Length(1 + 2),
                Constraint::Min(10),
                Constraint::Length(1 + 2),
            ]
            .as_ref(),
        )
        .split(f.size());

    /* f.size()
     * +----------------------------------------------------+
     * |                  chunks[0]                         |
     * +----+------------------------------------------+----+
     * |    |                                          |    |
     * |    |                                          |    |
     * | h  |                                          | h  |
     * | c  |                                          | c  |
     * | h  |                                          | h  |
     * | u  |             hchunks[1]                   | u  | chunks[1]
     * | n  |                                          | n  |
     * | k  |                                          | k  |
     * | s  |                                          | s  |
     * |[0] |                                          |[2] |
     * |    |                                          |    |
     * |    |                                          |    |
     * +----+------------------------------------------+----+
     * |                  chunks[2]                         |
     * +----------------------------------------------------+
     */
    let hchunks = Layout::default()
        .direction(Direction::Horizontal)
        .constraints(
            [
                Constraint::Length(GAME_CARD_WIDTH as u16 + 2 + 2),
                Constraint::Min((3 * GAME_CARD_WIDTH) as u16),
                Constraint::Length(GAME_CARD_WIDTH as u16 + 2 + 2),
            ]
            .as_ref(),
        )
        .split(chunks[1]);

    GameChunks {
        player_left: hchunks[0],
        player_top: chunks[0],
        player_right: hchunks[2],
        player_bottom: chunks[2],
        table: hchunks[1],
    }
}

fn ui_dealing<B: Backend>(f: &mut Frame<B>, name: &[&str], hand: &[Vec<HiddenCard>]) {
    let game_chunks = calc_game_chunks(f);
    assert_eq!(name.len(), 4);
    assert_eq!(hand.len(), 4);
    let team_re = false;
    render_player_left_right(
        f,
        game_chunks.player_left,
        name[0],
        hand[0].as_slice(),
        team_re,
        None,
    );
    render_player_top_bottom(
        f,
        game_chunks.player_top,
        name[1],
        hand[1].as_slice(),
        team_re,
        None,
    );
    render_player_left_right(
        f,
        game_chunks.player_right,
        name[2],
        hand[2].as_slice(),
        team_re,
        None,
    );
    render_player_top_bottom(
        f,
        game_chunks.player_bottom,
        name[3],
        hand[3].as_slice(),
        team_re,
        None,
    );
}

#[allow(clippy::too_many_arguments)]
fn ui_playing<B: Backend, S: AsRef<str>>(
    f: &mut Frame<B>,
    name: &[S],
    hand: &[Vec<HiddenCard>],
    team_re: [bool; 4],
    list_state: &mut [Option<ListState>; 4],
    trick_cards: &[(&usize, &GameCard)],
    trick_winner_index: &usize,
    last: bool,
) {
    let game_chunks = calc_game_chunks(f);
    render_player_left_right(
        f,
        game_chunks.player_left,
        name[0].as_ref(),
        hand[0].as_slice(),
        team_re[0],
        list_state[0].as_mut(),
    );
    render_player_top_bottom(
        f,
        game_chunks.player_top,
        name[1].as_ref(),
        hand[1].as_slice(),
        team_re[1],
        list_state[1].as_mut(),
    );
    render_player_left_right(
        f,
        game_chunks.player_right,
        name[2].as_ref(),
        hand[2].as_slice(),
        team_re[2],
        list_state[2].as_mut(),
    );
    render_player_top_bottom(
        f,
        game_chunks.player_bottom,
        name[3].as_ref(),
        hand[3].as_slice(),
        team_re[3],
        list_state[3].as_mut(),
    );
    render_table(f, game_chunks.table, trick_cards, trick_winner_index, last);
}

fn render_player_left_right<B: Backend>(
    f: &mut Frame<B>,
    area: Rect,
    name: &str,
    hand: &[HiddenCard],
    team_re: bool,
    list_state: Option<&mut ListState>,
) {
    let cards: Vec<ListItem> = hand
        .iter()
        .map(|c| ListItem::new(format!(" {}", DisplayHiddenCard::from(c))))
        .collect();
    let mut block = Block::default().borders(Borders::ALL);
    if team_re {
        block = block.title(format!("{} (RE)", name));
    } else {
        block = block.title(name);
    }
    let list = List::new(cards)
        .block(block)
        .style(Style::default().fg(Color::White))
        .highlight_style(Style::default().add_modifier(Modifier::REVERSED));

    /* area
     * +----+
     * |    | chunk[0]
     * +----+
     * |card|
     * |card|
     * |card|
     * |card| chunk[1]
     * |card|
     * |card|
     * |card|
     * |card|
     * +----+
     * |    | chunk[2]
     * +----+
     */
    let hand_len = hand.len() as u16;
    let chunk = Layout::default()
        .direction(Direction::Vertical)
        .constraints(
            [
                Constraint::Length((area.height - hand_len - 2) / 2),
                Constraint::Length(hand_len + 2),
                Constraint::Min((area.height - hand_len - 2) / 2),
            ]
            .as_ref(),
        )
        .split(area);

    if let Some(list_state) = list_state {
        f.render_stateful_widget(list, chunk[1], list_state);
    } else {
        f.render_widget(list, chunk[1]);
    }
}

fn render_player_top_bottom<B: Backend>(
    f: &mut Frame<B>,
    area: Rect,
    name: &str,
    hand: &[HiddenCard],
    team_re: bool,
    list_state: Option<&mut ListState>,
) {
    let cards: Vec<ListItem> = hand
        .iter()
        .map(|c| ListItem::new(DisplayHiddenCard::from(c).to_string()))
        .collect();
    let mut block = Block::default().borders(Borders::ALL);
    if team_re {
        block = block.title(format!("{} (RE)", name));
    } else {
        block = block.title(name);
    }
    let list = List::new(cards)
        .block(block)
        .style(Style::default().fg(Color::White))
        .highlight_style(Style::default().add_modifier(Modifier::REVERSED))
        .sep_width(Some(0))
        .max_height(Some(1));

    /* area
     * +---------+-------------------------------+----------+
     * |         | card card card card card card |          |
     * +---------+-------------------------------+----------+
     *  chunk[0]   chunk[1]                        chunk[2]
     */
    let hand_width = (hand.len() * GAME_CARD_WIDTH).max(name.width()) as u16;
    let chunk = Layout::default()
        .direction(Direction::Horizontal)
        .constraints(
            [
                Constraint::Length((area.width - hand_width - 2) / 2),
                Constraint::Length(hand_width + 2),
                Constraint::Min((area.width - hand_width - 2) / 2),
            ]
            .as_ref(),
        )
        .split(area);

    if let Some(list_state) = list_state {
        f.render_stateful_widget(list, chunk[1], list_state);
    } else {
        f.render_widget(list, chunk[1]);
    }
}

fn render_table<B: Backend>(
    f: &mut Frame<B>,
    area: Rect,
    trick_cards: &[(&usize, &GameCard)],
    trick_winner_index: &usize,
    last: bool,
) {
    /* area
     * +------------------------------------------+
     * |                chunks[0]                 |
     * |                                          |
     * +------------------------------------------+
     * |                chunks[1]                 |
     * |                                          |
     * |                                          |
     * +------------------------------------------+
     * |                chunks[2]                 |
     * |                                          |
     * |                                          |
     * +------------------------------------------+
     */
    let chunks = Layout::default()
        .direction(Direction::Vertical)
        .constraints(
            [
                Constraint::Length((area.height - (3 + 2)) / 2),
                Constraint::Length(3 + 2),
                Constraint::Min((area.height - (3 + 2)) / 2),
            ]
            .as_ref(),
        )
        .split(area);

    /* area
     * +------------------------------------------+
     * |                chunks[0]                 |
     * |                                          |
     * +----------+--------------------+----------+
     * |          |                    |          |
     * |hchunks[0]|        [1]         |   [2]    | chunks[1]
     * |          |                    |          |
     * +----------+--------------------+----------+
     * |                chunks[2]                 |
     * |                                          |
     * |                                          |
     * +------------------------------------------+
     */
    let hchunks = Layout::default()
        .direction(Direction::Horizontal)
        .constraints(
            [
                Constraint::Length((area.width - (3 * GAME_CARD_WIDTH + 2) as u16) / 2),
                Constraint::Length((3 * GAME_CARD_WIDTH + 2) as u16),
                Constraint::Min((area.width - (3 * GAME_CARD_WIDTH + 2) as u16) / 2),
            ]
            .as_ref(),
        )
        .split(chunks[1]);

    let table_title = if last { "last table" } else { "table" };
    let table_block = Block::default()
        .title(table_title)
        .borders(Borders::ALL)
        .style(Style::default().fg(Color::White));

    if trick_cards.is_empty() {
        f.render_widget(table_block, hchunks[1]);
    } else {
        /* area
         * +------------------------------------------+
         * |                chunks[0]                 |
         * |                                          |
         * +----------+------+------+------+----------+
         * |          |      |      |      |          |
         * |          |htable|htable|htable|          |
         * |hchunks[0]|chunks|chunks|chunks|hchunks[2]| chunks[1]
         * |          | [0]  | [1]  | [2]  |          |
         * |          |      |      |      |          |
         * +----------+------+------+------+----------+
         * |                chunks[2]                 |
         * |                                          |
         * |                                          |
         * +------------------------------------------+
         */
        let htable_chunks = Layout::default()
            .direction(Direction::Horizontal)
            .constraints(
                [
                    Constraint::Length(GAME_CARD_WIDTH as u16),
                    Constraint::Length(GAME_CARD_WIDTH as u16),
                    Constraint::Length(GAME_CARD_WIDTH as u16),
                ]
                .as_ref(),
            )
            .split(table_block.inner(hchunks[1]));

        /* area
         * +------------------------------------------+
         * |                chunks[0]                 |
         * |                                          |
         * +----------+------+------+------+----------+
         * |          | l[0] | m[0] | r[0] |          |
         * |          +------+------+------+          |
         * |hchunks[0]| l[1] | m[1] | r[1] |hchunks[2]| chunks[1]
         * |          +------+------+------+          |
         * |          | l[2] | m[2] | r[2] |          |
         * +----------+------+------+------+----------+
         * |                chunks[2]                 |
         * |                                          |
         * |                                          |
         * +------------------------------------------+
         */
        let lchunks = Layout::default()
            .direction(Direction::Vertical)
            .constraints(
                [
                    Constraint::Length(1),
                    Constraint::Length(1),
                    Constraint::Length(1),
                ]
                .as_ref(),
            )
            .split(htable_chunks[0]);
        let mchunks = Layout::default()
            .direction(Direction::Vertical)
            .constraints(
                [
                    Constraint::Length(1),
                    Constraint::Length(1),
                    Constraint::Length(1),
                ]
                .as_ref(),
            )
            .split(htable_chunks[1]);
        let rchunks = Layout::default()
            .direction(Direction::Vertical)
            .constraints(
                [
                    Constraint::Length(1),
                    Constraint::Length(1),
                    Constraint::Length(1),
                ]
                .as_ref(),
            )
            .split(htable_chunks[2]);

        let left = lchunks[1];
        let top = mchunks[0];
        let right = rchunks[1];
        let bottom = mchunks[2];

        let top_left = lchunks[0];
        let bottom_left = lchunks[2];
        let top_right = rchunks[0];
        let bottom_right = rchunks[2];

        f.render_widget(table_block, hchunks[1]);
        let mut first_card = true;
        for (index, card) in trick_cards {
            let mut card = Paragraph::new(DisplayGameCard::from(*card).to_string());
            if index == &trick_winner_index {
                card = card.style(Style::default().add_modifier(Modifier::REVERSED));
            }
            match index {
                0 => f.render_widget(card, left),
                1 => f.render_widget(card, top),
                2 => f.render_widget(card, right),
                3 => f.render_widget(card, bottom),
                _ => panic!("programming error detected"),
            }

            if first_card {
                first_card = false;
                match index {
                    0 => f.render_widget(Paragraph::new("   ↑"), bottom_left),
                    1 => f.render_widget(Paragraph::new("   ―→"), top_left),
                    2 => f.render_widget(Paragraph::new("   ↓"), top_right),
                    3 => f.render_widget(Paragraph::new("  ←―"), bottom_right),
                    _ => panic!("programming error detected"),
                }
            }
        }
    }
}

enum ScoringCategory {
    BasicPoints,
    Announcement,
    RefusalOfRe,
    RefusalOfKontra,
    ReWonWith,
    KontraWonWith,
    SpecialPoints,
}

#[derive(Clone, Copy, EnumIter)]
enum ScoringPoints {
    Gewonnen,
    Unter90,
    Unter60,
    Unter30,
    Schwarz,
    AnsageRe,
    AnsageKontra,
    AbsageReKeine90,
    AbsageReKeine60,
    AbsageReKeine30,
    AbsageReSchwarz,
    AbsageKontraKeine90,
    AbsageKontraKeine60,
    AbsageKontraKeine30,
    AbsageKontraSchwarz,
    Re120GegenKeine90,
    Re90GegenKeine60,
    Re60GegenKeine30,
    Re30GegenSchwarz,
    Kontra120GegenKeine90,
    Kontra90GegenKeine60,
    Kontra60GegenKeine30,
    Kontra30GegenSchwarz,
    GegenDieAlten,
    FuchsGefangen,
    KarlchenMueller,
    DoppelkopfStich,
}

impl From<ScoringPoints> for ScoringCategory {
    fn from(src: ScoringPoints) -> Self {
        match src {
            ScoringPoints::Gewonnen => Self::BasicPoints,
            ScoringPoints::Unter90 => Self::BasicPoints,
            ScoringPoints::Unter60 => Self::BasicPoints,
            ScoringPoints::Unter30 => Self::BasicPoints,
            ScoringPoints::Schwarz => Self::BasicPoints,
            ScoringPoints::AnsageRe => Self::Announcement,
            ScoringPoints::AnsageKontra => Self::Announcement,
            ScoringPoints::AbsageReKeine90 => Self::RefusalOfRe,
            ScoringPoints::AbsageReKeine60 => Self::RefusalOfRe,
            ScoringPoints::AbsageReKeine30 => Self::RefusalOfRe,
            ScoringPoints::AbsageReSchwarz => Self::RefusalOfRe,
            ScoringPoints::AbsageKontraKeine90 => Self::RefusalOfKontra,
            ScoringPoints::AbsageKontraKeine60 => Self::RefusalOfKontra,
            ScoringPoints::AbsageKontraKeine30 => Self::RefusalOfKontra,
            ScoringPoints::AbsageKontraSchwarz => Self::RefusalOfKontra,
            ScoringPoints::Re120GegenKeine90 => Self::ReWonWith,
            ScoringPoints::Re90GegenKeine60 => Self::ReWonWith,
            ScoringPoints::Re60GegenKeine30 => Self::ReWonWith,
            ScoringPoints::Re30GegenSchwarz => Self::ReWonWith,
            ScoringPoints::Kontra120GegenKeine90 => Self::KontraWonWith,
            ScoringPoints::Kontra90GegenKeine60 => Self::KontraWonWith,
            ScoringPoints::Kontra60GegenKeine30 => Self::KontraWonWith,
            ScoringPoints::Kontra30GegenSchwarz => Self::KontraWonWith,
            ScoringPoints::GegenDieAlten => Self::SpecialPoints,
            ScoringPoints::FuchsGefangen => Self::SpecialPoints,
            ScoringPoints::KarlchenMueller => Self::SpecialPoints,
            ScoringPoints::DoppelkopfStich => Self::SpecialPoints,
        }
    }
}

impl std::fmt::Display for ScoringCategory {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match &self {
            Self::BasicPoints => write!(f, "Basic Points"),
            Self::Announcement => write!(f, "Announcement"),
            Self::RefusalOfRe => write!(f, "Refusal of Re"),
            Self::RefusalOfKontra => write!(f, "Refusal of Kontra"),
            Self::ReWonWith => write!(f, "Re won with"),
            Self::KontraWonWith => write!(f, "Kontra won with"),
            Self::SpecialPoints => write!(f, "Special Points"),
        }
    }
}

impl std::fmt::Display for ScoringPoints {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match &self {
            Self::Gewonnen => write!(f, "won the game"),
            Self::Unter90 => write!(f, "under 90"),
            Self::Unter60 => write!(f, "under 60"),
            Self::Unter30 => write!(f, "under 30"),
            Self::Schwarz => write!(f, "Schwarz"),
            Self::AnsageRe => write!(f, "Re"),
            Self::AnsageKontra => write!(f, "Kontra"),
            Self::AbsageReKeine90 => write!(f, "no 90"),
            Self::AbsageReKeine60 => write!(f, "no 60"),
            Self::AbsageReKeine30 => write!(f, "no 30"),
            Self::AbsageReSchwarz => write!(f, "Schwarz"),
            Self::AbsageKontraKeine90 => write!(f, "no 90"),
            Self::AbsageKontraKeine60 => write!(f, "no 60"),
            Self::AbsageKontraKeine30 => write!(f, "no 30"),
            Self::AbsageKontraSchwarz => write!(f, "Schwarz"),
            Self::Re120GegenKeine90 => write!(f, "≥120 against 'no 90'"),
            Self::Re90GegenKeine60 => write!(f, "≥90 against 'no 60'"),
            Self::Re60GegenKeine30 => write!(f, "≥60 against 'no 30'"),
            Self::Re30GegenSchwarz => write!(f, "≥30 against 'Schwarz'"),
            Self::Kontra120GegenKeine90 => write!(f, "≥120 against 'no 90'"),
            Self::Kontra90GegenKeine60 => write!(f, "≥90 against 'no 60'"),
            Self::Kontra60GegenKeine30 => write!(f, "≥60 against 'no 30'"),
            Self::Kontra30GegenSchwarz => write!(f, "≥30 against 'Schwarz'"),
            Self::GegenDieAlten => write!(f, "won against the elders"),
            Self::FuchsGefangen => write!(f, "catching of fox"),
            Self::KarlchenMueller => write!(f, "charlie makes last trick"),
            Self::DoppelkopfStich => write!(f, "doppelkopf trick"),
        }
    }
}

fn ui_scoring_game_value<B: Backend>(
    f: &mut Frame<B>,
    points: &[(ScoringPoints, Option<bool>, Option<bool>)],
    game_value: (usize, usize, usize),
) {
    let mut table = Vec::with_capacity(points.len() + 1); // +1 for game value row
    let val = |p| match p {
        None => "-",
        Some(false) => "0",
        Some(true) => "1",
    };
    for p in points {
        let val_re = val(p.1);
        let val_kontra = val(p.2);
        let row = Row::new(vec![
            ScoringCategory::from(p.0).to_string(),
            p.0.to_string(),
            val_re.to_owned(),
            val_kontra.to_owned(),
        ]);
        table.push(row);
    }
    let last = table.pop().unwrap().bottom_margin(1);
    table.push(last);

    table.push(Row::new(vec![
        "Game Value".to_owned(),
        "".to_owned(),
        game_value.0.to_string(),
        game_value.1.to_string(),
    ]));
    if game_value.0 > game_value.1 {
        table.push(Row::new(vec![
            "Game Value".to_owned(),
            "".to_owned(),
            game_value.2.to_string(),
            "".to_owned(),
        ]));
    } else {
        table.push(Row::new(vec![
            "Game Value".to_owned(),
            "".to_owned(),
            "".to_owned(),
            game_value.2.to_string(),
        ]));
    }

    let table = Table::new(table)
        .style(Style::default().fg(Color::White))
        .header(
            Row::new(vec!["Category", "Criterion", "Re", "Kontra"])
                .style(Style::default().fg(Color::Yellow))
                .bottom_margin(1),
        )
        .block(Block::default().title("Game Value").borders(Borders::ALL))
        .widths(&[
            Constraint::Length(17),
            Constraint::Length(22),
            Constraint::Length(6),
            Constraint::Length(6),
        ])
        .column_spacing(1);

    f.render_widget(table, f.size());
}

fn ui_scoring_table<B: Backend>(f: &mut Frame<B>, sm: &StateMachine<StateScoring>) {
    let player_names = sm.players.iter().map(|p| p.name()).collect::<Vec<_>>();
    let player_name_width = player_names.iter().fold(0, |a, pn| a.max(pn.width())) as u16;

    let player_scoring_sum = sm.scoring_table.iter().fold([0; 4], |acc, row| {
        [
            acc[0] + row.player_scoring[0],
            acc[1] + row.player_scoring[1],
            acc[2] + row.player_scoring[2],
            acc[3] + row.player_scoring[3],
        ]
    });
    let mut table = sm
        .scoring_table
        .iter()
        .enumerate()
        .map(|(i, row)| {
            Row::new(vec![
                (i + 1).to_string(),
                row.game_value.to_string(),
                row.player_scoring[0].to_string(),
                row.player_scoring[1].to_string(),
                row.player_scoring[2].to_string(),
                row.player_scoring[3].to_string(),
            ])
        })
        .collect::<Vec<_>>();
    let last = table.pop().unwrap().bottom_margin(1);
    table.push(last);
    table.push(Row::new(vec![
        "∑".to_owned(), //< sum
        "".to_owned(),
        player_scoring_sum[0].to_string(),
        player_scoring_sum[1].to_string(),
        player_scoring_sum[2].to_string(),
        player_scoring_sum[3].to_string(),
    ]));

    // need to be separate because it refers a runtime variable
    let widths = &[
        Constraint::Length(5),
        Constraint::Length(10),
        Constraint::Length(player_name_width),
        Constraint::Length(player_name_width),
        Constraint::Length(player_name_width),
        Constraint::Length(player_name_width),
    ];
    let table = Table::new(table)
        .style(Style::default().fg(Color::White))
        .header(
            Row::new(vec![
                "Round",
                "Game Value",
                player_names[0],
                player_names[1],
                player_names[2],
                player_names[3],
            ])
            .style(Style::default().fg(Color::Yellow))
            .bottom_margin(1),
        )
        .block(
            Block::default()
                .title("Scoring Table")
                .borders(Borders::ALL),
        )
        .widths(widths)
        .column_spacing(1);

    f.render_widget(table, f.size());
}

struct StateIdle {}
struct StateDealing {
    dealer: Dealer,
}
struct StatePlayingNormal {
    /// vector of winner index and table cards (player index and played card)
    played_tricks: Vec<(usize, [(usize, GameCard); 4])>,
    team_re: Vec<usize>,
    player_states: [Option<ListState>; 4],
}
struct StateScoring {
    played_tricks: Vec<(usize, [(usize, GameCard); 4])>,
    team_re: Vec<usize>,
    team_kontra: Vec<usize>,
}

struct ScoringRound {
    game_value: usize,
    player_scoring: [isize; 4],
}

struct StateMachine<S> {
    players: [Box<dyn Player>; 4],
    scoring_table: Vec<ScoringRound>,
    state: S,
}

impl StateMachine<StateIdle> {
    fn new(ouvert: bool, ki_only: bool) -> Self {
        let players: [Box<dyn Player>; 4] = if ki_only {
            [
                create_player::<KIPlayer>("Aria", ouvert),
                create_player::<KIPlayer>("Beau", ouvert),
                create_player::<KIPlayer>("Cora", ouvert),
                create_player::<KIPlayer>("Dean", true),
            ]
        } else {
            [
                create_player::<KIPlayer>("Aria", ouvert),
                create_player::<KIPlayer>("Beau", ouvert),
                create_player::<KIPlayer>("Cora", ouvert),
                create_player::<TUIPlayer>("ME", true),
            ]
        };

        Self {
            players,
            scoring_table: Vec::new(),
            state: StateIdle {},
        }
    }
}

impl From<StateMachine<StateIdle>> for StateMachine<StateDealing> {
    fn from(s: StateMachine<StateIdle>) -> Self {
        Self {
            players: s.players,
            scoring_table: s.scoring_table,
            state: StateDealing {
                dealer: Dealer::new(),
            },
        }
    }
}

impl From<StateMachine<StateDealing>> for StateMachine<StatePlayingNormal> {
    fn from(s: StateMachine<StateDealing>) -> Self {
        let mut player_states: [Option<ListState>; 4] = [None, None, None, None];
        for (player, player_state) in s.players.iter().zip(player_states.iter_mut()) {
            if player.is_interactive() {
                let s = ListState::default();
                assert_eq!(s.selected(), None);
                *player_state = Some(s);
            }
        }

        Self {
            players: s.players,
            scoring_table: s.scoring_table,
            state: StatePlayingNormal {
                played_tricks: Vec::with_capacity(10),
                team_re: Vec::with_capacity(2),
                player_states,
            },
        }
    }
}

impl From<StateMachine<StatePlayingNormal>> for StateMachine<StateScoring> {
    fn from(s: StateMachine<StatePlayingNormal>) -> Self {
        let mut team_kontra: Vec<_> = (0..4).collect();
        team_kontra.retain(|i| !s.state.team_re.contains(i));
        Self {
            players: s.players,
            scoring_table: s.scoring_table,
            state: StateScoring {
                played_tricks: s.state.played_tricks,
                team_re: s.state.team_re,
                team_kontra,
            },
        }
    }
}

impl From<StateMachine<StateScoring>> for StateMachine<StateDealing> {
    fn from(s: StateMachine<StateScoring>) -> Self {
        Self {
            players: s.players,
            scoring_table: s.scoring_table,
            state: StateDealing {
                dealer: Dealer::new(),
            },
        }
    }
}

#[cfg(test)]
mod test {
    use doppelkopf_lib::gamecard::GameCard;
    use doppelkopf_lib::rank::Rank;
    use doppelkopf_lib::suit::Suit;

    #[test]
    fn has_doppelkopf_stich() {
        let played_tricks = [(
            0,
            [
                (0, GameCard::new(&Suit::Club, &Rank::Ace)),
                (1, GameCard::new(&Suit::Club, &Rank::Ace)),
                (2, GameCard::new(&Suit::Club, &Rank::Val10)),
                (3, GameCard::new(&Suit::Club, &Rank::Val10)),
            ],
        )];
        assert!(super::has_doppelkopf_stich(played_tricks.as_ref(), &[0]));
        assert!(!super::has_doppelkopf_stich(played_tricks.as_ref(), &[1]));
        assert!(!super::has_doppelkopf_stich(played_tricks.as_ref(), &[2]));
        assert!(!super::has_doppelkopf_stich(played_tricks.as_ref(), &[3]));
    }

    #[test]
    fn has_not_doppelkopf_stich() {
        let played_tricks = [(
            2,
            [
                (0, GameCard::new(&Suit::Club, &Rank::Ace)),
                (1, GameCard::new(&Suit::Club, &Rank::Ace)),
                (2, GameCard::new(&Suit::Heart, &Rank::Val10)),
                (3, GameCard::new(&Suit::Diamond, &Rank::King)),
            ],
        )];
        assert!(!super::has_doppelkopf_stich(played_tricks.as_ref(), &[0]));
        assert!(!super::has_doppelkopf_stich(played_tricks.as_ref(), &[1]));
        assert!(!super::has_doppelkopf_stich(played_tricks.as_ref(), &[2]));
        assert!(!super::has_doppelkopf_stich(played_tricks.as_ref(), &[3]));
    }

    #[test]
    fn team_value_onecard_42() {
        let played_tricks = [(
            0,
            [
                (0, GameCard::new(&Suit::Club, &Rank::Ace)),
                (1, GameCard::new(&Suit::Club, &Rank::Ace)),
                (2, GameCard::new(&Suit::Club, &Rank::Val10)),
                (3, GameCard::new(&Suit::Club, &Rank::Val10)),
            ],
        )];
        let val = 42;
        assert_eq!(super::team_value(played_tricks.as_ref(), &[0]), val);
        assert_eq!(super::team_value(played_tricks.as_ref(), &[1]), 0);
        assert_eq!(super::team_value(played_tricks.as_ref(), &[2]), 0);
        assert_eq!(super::team_value(played_tricks.as_ref(), &[3]), 0);
        assert_eq!(super::team_value(played_tricks.as_ref(), &[0, 1]), val);
        assert_eq!(super::team_value(played_tricks.as_ref(), &[0, 2]), val);
        assert_eq!(super::team_value(played_tricks.as_ref(), &[0, 3]), val);
        assert_eq!(super::team_value(played_tricks.as_ref(), &[1, 0]), val);
        assert_eq!(super::team_value(played_tricks.as_ref(), &[1, 2]), 0);
        assert_eq!(super::team_value(played_tricks.as_ref(), &[1, 3]), 0);
        assert_eq!(super::team_value(played_tricks.as_ref(), &[2, 0]), val);
        assert_eq!(super::team_value(played_tricks.as_ref(), &[2, 1]), 0);
        assert_eq!(super::team_value(played_tricks.as_ref(), &[2, 3]), 0);
        assert_eq!(super::team_value(played_tricks.as_ref(), &[3, 0]), val);
        assert_eq!(super::team_value(played_tricks.as_ref(), &[3, 1]), 0);
        assert_eq!(super::team_value(played_tricks.as_ref(), &[3, 2]), 0);
    }

    #[test]
    fn team_value_onecard_36() {
        let played_tricks = [(
            2,
            [
                (0, GameCard::new(&Suit::Club, &Rank::Ace)),
                (1, GameCard::new(&Suit::Club, &Rank::Ace)),
                (2, GameCard::new(&Suit::Heart, &Rank::Val10)),
                (3, GameCard::new(&Suit::Diamond, &Rank::King)),
            ],
        )];
        let val = 36;
        assert_eq!(super::team_value(played_tricks.as_ref(), &[0]), 0);
        assert_eq!(super::team_value(played_tricks.as_ref(), &[1]), 0);
        assert_eq!(super::team_value(played_tricks.as_ref(), &[2]), val);
        assert_eq!(super::team_value(played_tricks.as_ref(), &[3]), 0);
        assert_eq!(super::team_value(played_tricks.as_ref(), &[0, 1]), 0);
        assert_eq!(super::team_value(played_tricks.as_ref(), &[0, 2]), val);
        assert_eq!(super::team_value(played_tricks.as_ref(), &[0, 3]), 0);
        assert_eq!(super::team_value(played_tricks.as_ref(), &[1, 0]), 0);
        assert_eq!(super::team_value(played_tricks.as_ref(), &[1, 2]), val);
        assert_eq!(super::team_value(played_tricks.as_ref(), &[1, 3]), 0);
        assert_eq!(super::team_value(played_tricks.as_ref(), &[2, 0]), val);
        assert_eq!(super::team_value(played_tricks.as_ref(), &[2, 1]), val);
        assert_eq!(super::team_value(played_tricks.as_ref(), &[2, 3]), val);
        assert_eq!(super::team_value(played_tricks.as_ref(), &[3, 0]), 0);
        assert_eq!(super::team_value(played_tricks.as_ref(), &[3, 1]), 0);
        assert_eq!(super::team_value(played_tricks.as_ref(), &[3, 2]), val);
    }

    #[test]
    fn team_value_twocards() {
        let played_tricks = [
            (
                0,
                [
                    (0, GameCard::new(&Suit::Club, &Rank::Ace)),
                    (1, GameCard::new(&Suit::Club, &Rank::Ace)),
                    (2, GameCard::new(&Suit::Club, &Rank::Val10)),
                    (3, GameCard::new(&Suit::Club, &Rank::Val10)),
                ],
            ),
            (
                2,
                [
                    (0, GameCard::new(&Suit::Club, &Rank::Ace)),
                    (1, GameCard::new(&Suit::Club, &Rank::Ace)),
                    (2, GameCard::new(&Suit::Heart, &Rank::Val10)),
                    (3, GameCard::new(&Suit::Diamond, &Rank::King)),
                ],
            ),
        ];
        let val_one = 42;
        let val_two = 36;
        let val_all = val_one + val_two;
        assert_eq!(super::team_value(played_tricks.as_ref(), &[0]), val_one);
        assert_eq!(super::team_value(played_tricks.as_ref(), &[1]), 0);
        assert_eq!(super::team_value(played_tricks.as_ref(), &[2]), val_two);
        assert_eq!(super::team_value(played_tricks.as_ref(), &[3]), 0);
        assert_eq!(super::team_value(played_tricks.as_ref(), &[0, 1]), val_one);
        assert_eq!(super::team_value(played_tricks.as_ref(), &[0, 2]), val_all);
        assert_eq!(super::team_value(played_tricks.as_ref(), &[0, 3]), val_one);
        assert_eq!(super::team_value(played_tricks.as_ref(), &[1, 0]), val_one);
        assert_eq!(super::team_value(played_tricks.as_ref(), &[1, 2]), val_two);
        assert_eq!(super::team_value(played_tricks.as_ref(), &[1, 3]), 0);
        assert_eq!(super::team_value(played_tricks.as_ref(), &[2, 0]), val_all);
        assert_eq!(super::team_value(played_tricks.as_ref(), &[2, 1]), val_two);
        assert_eq!(super::team_value(played_tricks.as_ref(), &[2, 3]), val_two);
        assert_eq!(super::team_value(played_tricks.as_ref(), &[3, 0]), val_one);
        assert_eq!(super::team_value(played_tricks.as_ref(), &[3, 1]), 0);
        assert_eq!(super::team_value(played_tricks.as_ref(), &[3, 2]), val_two);
    }

    #[test]
    fn has_club_jack_made_last_trick_onecard() {
        let played_tricks = [(
            0,
            [
                (0, GameCard::new(&Suit::Club, &Rank::Jack)),
                (1, GameCard::new(&Suit::Club, &Rank::Ace)),
                (2, GameCard::new(&Suit::Club, &Rank::Val10)),
                (3, GameCard::new(&Suit::Club, &Rank::Val10)),
            ],
        )];
        assert!(super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[0]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[1]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[2]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[3]
        ));
        assert!(super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[0, 1]
        ));
        assert!(super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[0, 2]
        ));
        assert!(super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[0, 3]
        ));
        assert!(super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[1, 0]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[1, 2]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[1, 3]
        ));
        assert!(super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[2, 0]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[2, 1]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[2, 3]
        ));
        assert!(super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[3, 0]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[3, 1]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[3, 2]
        ));
    }

    #[test]
    fn has_club_jack_made_last_trick_twocard() {
        let played_tricks = [
            (
                0,
                [
                    (0, GameCard::new(&Suit::Club, &Rank::Ace)),
                    (1, GameCard::new(&Suit::Club, &Rank::Ace)),
                    (2, GameCard::new(&Suit::Club, &Rank::Val10)),
                    (3, GameCard::new(&Suit::Club, &Rank::Val10)),
                ],
            ),
            (
                0,
                [
                    (0, GameCard::new(&Suit::Club, &Rank::Jack)),
                    (1, GameCard::new(&Suit::Club, &Rank::Ace)),
                    (2, GameCard::new(&Suit::Club, &Rank::Val10)),
                    (3, GameCard::new(&Suit::Club, &Rank::Val10)),
                ],
            ),
        ];
        assert!(super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[0]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[1]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[2]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[3]
        ));
        assert!(super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[0, 1]
        ));
        assert!(super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[0, 2]
        ));
        assert!(super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[0, 3]
        ));
        assert!(super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[1, 0]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[1, 2]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[1, 3]
        ));
        assert!(super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[2, 0]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[2, 1]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[2, 3]
        ));
        assert!(super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[3, 0]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[3, 1]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[3, 2]
        ));
    }

    #[test]
    fn has_not_club_jack_made_last_trick_nojack() {
        let played_tricks = [(
            0,
            [
                (0, GameCard::new(&Suit::Club, &Rank::Ace)),
                (1, GameCard::new(&Suit::Club, &Rank::Ace)),
                (2, GameCard::new(&Suit::Club, &Rank::Val10)),
                (3, GameCard::new(&Suit::Club, &Rank::Val10)),
            ],
        )];
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[0]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[1]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[2]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[3]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[0, 1]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[0, 2]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[0, 3]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[1, 0]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[1, 2]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[1, 3]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[2, 0]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[2, 1]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[2, 3]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[3, 0]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[3, 1]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[3, 2]
        ));
    }

    #[test]
    fn has_not_club_jack_made_last_trick_jack() {
        let played_tricks = [(
            0,
            [
                // charlie miller was catched
                (0, GameCard::new(&Suit::Heart, &Rank::Val10)),
                (1, GameCard::new(&Suit::Club, &Rank::Jack)),
                (2, GameCard::new(&Suit::Club, &Rank::Val10)),
                (3, GameCard::new(&Suit::Club, &Rank::Val10)),
            ],
        )];
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[0]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[1]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[2]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[3]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[0, 1]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[0, 2]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[0, 3]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[1, 0]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[1, 2]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[1, 3]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[2, 0]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[2, 1]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[2, 3]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[3, 0]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[3, 1]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[3, 2]
        ));
    }

    #[test]
    fn has_not_club_jack_made_last_trick_notlast() {
        let played_tricks = [
            (
                1,
                [
                    // not last trick
                    (0, GameCard::new(&Suit::Spade, &Rank::Ace)),
                    (1, GameCard::new(&Suit::Club, &Rank::Jack)),
                    (2, GameCard::new(&Suit::Spade, &Rank::Val10)),
                    (3, GameCard::new(&Suit::Spade, &Rank::Val10)),
                ],
            ),
            (
                0,
                [
                    (0, GameCard::new(&Suit::Heart, &Rank::Val10)),
                    (1, GameCard::new(&Suit::Club, &Rank::Ace)),
                    (2, GameCard::new(&Suit::Club, &Rank::Val10)),
                    (3, GameCard::new(&Suit::Club, &Rank::Val10)),
                ],
            ),
        ];
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[0]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[1]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[2]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[3]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[0, 1]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[0, 2]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[0, 3]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[1, 0]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[1, 2]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[1, 3]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[2, 0]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[2, 1]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[2, 3]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[3, 0]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[3, 1]
        ));
        assert!(!super::has_club_jack_made_last_trick(
            played_tricks.as_ref(),
            &[3, 2]
        ));
    }

    #[test]
    fn has_fox_catched_onecard() {
        let fox = GameCard::new(&Suit::Diamond, &Rank::Ace);
        let played_tricks = [(
            0,
            [
                (0, GameCard::new(&Suit::Heart, &Rank::Val10)),
                (1, GameCard::new(&Suit::Club, &Rank::Ace)),
                (2, fox),
                (3, GameCard::new(&Suit::Club, &Rank::Val10)),
            ],
        )];
        assert!(super::has_fox_catched(played_tricks.as_ref(), &[0]));
        assert!(!super::has_fox_catched(played_tricks.as_ref(), &[1]));
        assert!(!super::has_fox_catched(played_tricks.as_ref(), &[2]));
        assert!(!super::has_fox_catched(played_tricks.as_ref(), &[3]));
        assert!(super::has_fox_catched(played_tricks.as_ref(), &[0, 1]));
        assert!(!super::has_fox_catched(played_tricks.as_ref(), &[0, 2]));
        assert!(super::has_fox_catched(played_tricks.as_ref(), &[0, 3]));
        assert!(super::has_fox_catched(played_tricks.as_ref(), &[1, 0]));
        assert!(!super::has_fox_catched(played_tricks.as_ref(), &[1, 2]));
        assert!(!super::has_fox_catched(played_tricks.as_ref(), &[1, 3]));
        assert!(!super::has_fox_catched(played_tricks.as_ref(), &[2, 0]));
        assert!(!super::has_fox_catched(played_tricks.as_ref(), &[2, 1]));
        assert!(!super::has_fox_catched(played_tricks.as_ref(), &[2, 3]));
        assert!(super::has_fox_catched(played_tricks.as_ref(), &[3, 0]));
        assert!(!super::has_fox_catched(played_tricks.as_ref(), &[3, 1]));
        assert!(!super::has_fox_catched(played_tricks.as_ref(), &[3, 2]));
    }

    #[test]
    fn has_fox_catched_threecards() {
        let fox = GameCard::new(&Suit::Diamond, &Rank::Ace);
        let played_tricks = [
            (
                2,
                [
                    (0, GameCard::new(&Suit::Club, &Rank::Ace)),
                    (1, GameCard::new(&Suit::Club, &Rank::Ace)),
                    (2, GameCard::new(&Suit::Heart, &Rank::Val10)),
                    (3, GameCard::new(&Suit::Club, &Rank::Val10)),
                ],
            ),
            (
                1,
                [
                    (2, fox),
                    (3, GameCard::new(&Suit::Spade, &Rank::Val10)),
                    (0, GameCard::new(&Suit::Spade, &Rank::Ace)),
                    (1, GameCard::new(&Suit::Heart, &Rank::Val10)),
                ],
            ),
            (
                3,
                [
                    (1, GameCard::new(&Suit::Spade, &Rank::Ace)),
                    (2, GameCard::new(&Suit::Club, &Rank::King)),
                    (3, GameCard::new(&Suit::Club, &Rank::Queen)),
                    (0, GameCard::new(&Suit::Spade, &Rank::Val10)),
                ],
            ),
        ];
        assert!(!super::has_fox_catched(played_tricks.as_ref(), &[0]));
        assert!(super::has_fox_catched(played_tricks.as_ref(), &[1]));
        assert!(!super::has_fox_catched(played_tricks.as_ref(), &[2]));
        assert!(!super::has_fox_catched(played_tricks.as_ref(), &[3]));
        assert!(super::has_fox_catched(played_tricks.as_ref(), &[0, 1]));
        assert!(!super::has_fox_catched(played_tricks.as_ref(), &[0, 2]));
        assert!(!super::has_fox_catched(played_tricks.as_ref(), &[0, 3]));
        assert!(super::has_fox_catched(played_tricks.as_ref(), &[1, 0]));
        assert!(!super::has_fox_catched(played_tricks.as_ref(), &[1, 2]));
        assert!(super::has_fox_catched(played_tricks.as_ref(), &[1, 3]));
        assert!(!super::has_fox_catched(played_tricks.as_ref(), &[2, 0]));
        assert!(!super::has_fox_catched(played_tricks.as_ref(), &[2, 1]));
        assert!(!super::has_fox_catched(played_tricks.as_ref(), &[2, 3]));
        assert!(!super::has_fox_catched(played_tricks.as_ref(), &[3, 0]));
        assert!(super::has_fox_catched(played_tricks.as_ref(), &[3, 1]));
        assert!(!super::has_fox_catched(played_tricks.as_ref(), &[3, 2]));
    }
}
