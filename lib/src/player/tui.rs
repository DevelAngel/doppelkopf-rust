use crate::announcement::VorbehaltAnnouncement;
use crate::card::Card;
use crate::gamecard::GameCard;
use crate::hiddencard::HiddenCard;
use crate::rank::Rank;
use crate::suit::Suit;

use std::collections::VecDeque;

use uuid::{Uuid, UuidBytes};

pub struct TUIPlayer {
    uuid: Uuid,
    name: String,
    hand: VecDeque<GameCard>,
}

impl TUIPlayer {
    fn sort_hand(&mut self) {
        self.hand
            .make_contiguous()
            .sort_by(|a, b| a.cmp(b).reverse());
    }
}

impl super::GamePlayer for TUIPlayer {
    fn new(name: &str, _ouvert: bool) -> Self
    where
        Self: Sized,
    {
        let uuid = Uuid::new_v4();
        Self {
            uuid,
            name: name.to_string(),
            hand: VecDeque::with_capacity(12),
        }
    }
}

impl super::Player for TUIPlayer {
    fn is_interactive(&self) -> bool {
        true
    }

    fn uuid(&self) -> &UuidBytes {
        self.uuid.as_bytes()
    }

    fn name(&self) -> &str {
        &self.name
    }

    fn hand_len(&self) -> usize {
        self.hand.len()
    }

    fn hand(&self) -> Vec<HiddenCard> {
        //vec![HiddenCard::hidden(); self.hand.len()]
        self.hand
            .iter()
            .map(|c| HiddenCard::shown(c.card().clone()))
            .collect()
    }

    fn take_three_cards(&mut self, cards: [GameCard; 3]) {
        for c in cards {
            self.hand.push_back(c);
        }

        self.sort_hand();
    }

    fn take_four_cards(&mut self, cards: [GameCard; 4]) {
        for c in cards {
            self.hand.push_back(c);
        }

        self.sort_hand();
    }

    fn announce_vorbehalt(&self) -> VorbehaltAnnouncement {
        assert_eq!(self.hand.len(), 10);
        let num_queens = self
            .hand
            .iter()
            .filter(|c| c.card() == &Card::new(&Suit::Club, &Rank::Queen))
            .count();
        match num_queens {
            0 | 1 => VorbehaltAnnouncement::Gesund,
            2 => VorbehaltAnnouncement::Vorbehalt,
            _ => panic!("too much queens: {} club queens", num_queens),
        }
    }

    fn play_card(
        &mut self,
        played: &[GameCard],
        f: &mut dyn FnMut(&[usize]) -> Option<usize>,
    ) -> GameCard {
        let index: usize = match played.first() {
            None => {
                let indices: Vec<usize> = (0..self.hand.len()).collect();
                f(indices.as_slice()).expect("cannot play first card")
            }
            Some(first_card) => {
                let (indices_following, cards_following): (Vec<_>, Vec<_>) = self
                    .hand
                    .iter()
                    .enumerate()
                    .filter(|&(_, c)| c.card().following(first_card.card()))
                    .unzip();
                assert_eq!(indices_following.len(), cards_following.len());
                if indices_following.is_empty() {
                    // no following cards
                    let indices: Vec<usize> = (0..self.hand.len()).collect();
                    f(indices.as_slice()).expect("cannot play a non-following card")
                } else {
                    // select one of following cards
                    f(indices_following.as_slice()).expect("cannot play a following card")
                }
            }
        };
        self.hand
            .remove(index)
            .unwrap_or_else(|| panic!("wrong index: {}", index))
    }
}
