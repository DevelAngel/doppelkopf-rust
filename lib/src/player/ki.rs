use crate::announcement::VorbehaltAnnouncement;
use crate::card::Card;
use crate::gamecard::GameCard;
use crate::hiddencard::HiddenCard;
use crate::rank::Rank;
use crate::suit::Suit;

use std::collections::VecDeque;

use uuid::{Uuid, UuidBytes};

pub struct KIPlayer {
    uuid: Uuid,
    name: String,
    ouvert: bool,
    hand: VecDeque<GameCard>,
}

impl KIPlayer {
    fn sort_hand(&mut self) {
        self.hand
            .make_contiguous()
            .sort_by(|a, b| a.cmp(b).reverse());
    }
}

impl super::GamePlayer for KIPlayer {
    fn new(name: &str, ouvert: bool) -> Self
    where
        Self: Sized,
    {
        let uuid = Uuid::new_v4();
        Self {
            uuid,
            name: name.to_string(),
            ouvert,
            hand: VecDeque::with_capacity(12),
        }
    }
}

impl super::Player for KIPlayer {
    fn is_interactive(&self) -> bool {
        false
    }

    fn uuid(&self) -> &UuidBytes {
        self.uuid.as_bytes()
    }

    fn name(&self) -> &str {
        &self.name
    }

    fn hand_len(&self) -> usize {
        self.hand.len()
    }

    fn hand(&self) -> Vec<HiddenCard> {
        if self.ouvert {
            self.hand
                .iter()
                .map(|c| HiddenCard::shown(c.card().clone()))
                .collect()
        } else {
            vec![HiddenCard::hidden(); self.hand.len()]
        }
    }

    fn take_three_cards(&mut self, cards: [GameCard; 3]) {
        for c in cards {
            self.hand.push_back(c);
        }

        self.sort_hand();
    }

    fn take_four_cards(&mut self, cards: [GameCard; 4]) {
        for c in cards {
            self.hand.push_back(c);
        }

        self.sort_hand();
    }

    fn announce_vorbehalt(&self) -> VorbehaltAnnouncement {
        assert_eq!(self.hand.len(), 10);
        let num_queens = self
            .hand
            .iter()
            .filter(|c| c.card() == &Card::new(&Suit::Club, &Rank::Queen))
            .count();
        match num_queens {
            0 | 1 => VorbehaltAnnouncement::Gesund,
            2 => VorbehaltAnnouncement::Vorbehalt,
            _ => panic!("too much queens: {} club queens", num_queens),
        }
    }

    fn play_card(
        &mut self,
        played: &[GameCard],
        _f: &mut dyn FnMut(&[usize]) -> Option<usize>,
    ) -> GameCard {
        let index: usize = match played.first() {
            None => 0, // todo
            Some(first_card) => {
                let mut iter = self
                    .hand
                    .iter()
                    .enumerate()
                    .filter(|&(_, c)| c.card().following(first_card.card()));
                match iter.next() {
                    None => 0,         // no following cards
                    Some((i, _)) => i, // play first following card
                }
            }
        };
        self.hand
            .remove(index)
            .unwrap_or_else(|| panic!("wrong index: {}", index))
    }
}
