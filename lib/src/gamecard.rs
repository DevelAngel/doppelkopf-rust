use crate::card::Card;
use crate::rank::Rank;
use crate::suit::Suit;

use std::cmp::Ordering;

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct GameCard {
    card: Card,
}

impl GameCard {
    #[allow(dead_code)]
    pub fn new(suit: &Suit, rank: &Rank) -> Self {
        Self {
            card: Card::new(suit, rank),
        }
    }

    #[allow(dead_code)]
    pub(crate) fn new_card(card: &Card) -> Self {
        Self { card: card.clone() }
    }

    pub fn card(&self) -> &Card {
        &self.card
    }
}

impl From<&GameCard> for Card {
    fn from(src: &GameCard) -> Self {
        src.card().clone()
    }
}

impl PartialOrd for GameCard {
    fn partial_cmp(&self, other: &GameCard) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for GameCard {
    fn cmp(&self, other: &Self) -> Ordering {
        match self.card().partial_cmp(other.card()) {
            Some(ord) => ord,
            None => {
                if self.card().is_trump() {
                    Ordering::Greater
                } else {
                    let self_suit = self.card().suit();
                    let other_suit = other.card().suit();
                    match (self_suit, other_suit) {
                        (Suit::Club, _) => Ordering::Greater,
                        (_, Suit::Club) => Ordering::Less,
                        (Suit::Spade, _) => Ordering::Greater,
                        (_, Suit::Spade) => Ordering::Less,
                        (Suit::Heart, _) => Ordering::Greater,
                        (_, Suit::Heart) => Ordering::Less,
                        _ => panic!(
                            "unsupported case detected: {} cmp {}",
                            self_suit, other_suit
                        ),
                    }
                }
            }
        }
    }
}
