use crate::rank::Rank;
use crate::suit::Suit;

use std::cmp::Ordering;

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Card {
    suit: Suit,
    rank: Rank,
}

impl Card {
    pub fn new(suit: &Suit, rank: &Rank) -> Self {
        Self {
            suit: suit.clone(),
            rank: rank.clone(),
        }
    }

    pub fn suit(&self) -> &Suit {
        &self.suit
    }

    pub fn rank(&self) -> &Rank {
        &self.rank
    }

    pub fn value(&self) -> usize {
        match self.rank {
            Rank::Ace => 11,
            Rank::Val10 => 10,
            Rank::King => 4,
            Rank::Queen => 3,
            Rank::Jack => 2,
            #[allow(unreachable_patterns)]
            _ => 0,
        }
    }

    pub fn is_trump(&self) -> bool {
        match (&self.suit, &self.rank) {
            (Suit::Heart, Rank::Val10) => true,
            (Suit::Diamond, _) => true,
            (_, Rank::Queen) => true,
            (_, Rank::Jack) => true,
            (_, _) => false,
        }
    }

    pub fn following(&self, other: &Self) -> bool {
        let self_trump = self.is_trump();
        let other_trump = other.is_trump();
        if self_trump || other_trump {
            self_trump == other_trump
        } else {
            other.suit() == &self.suit
        }
    }
}

impl PartialOrd for Card {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        match (self.is_trump(), other.is_trump()) {
            (true, false) => Some(Ordering::Greater),
            (false, true) => Some(Ordering::Less),
            (false, false) => {
                if self.suit == other.suit {
                    match (&self.rank, &other.rank) {
                        (Rank::Ace, Rank::Ace) => Some(Ordering::Equal),
                        (Rank::Ace, _) => Some(Ordering::Greater),
                        (Rank::Val10, Rank::Ace) => Some(Ordering::Less),
                        (Rank::Val10, Rank::Val10) => Some(Ordering::Equal),
                        (Rank::Val10, Rank::King) => Some(Ordering::Greater),
                        (Rank::King, Rank::King) => Some(Ordering::Equal),
                        (Rank::King, _) => Some(Ordering::Less),
                        (_, _) => None, // illegal cases
                    }
                } else {
                    // we cannot compare two different non-trump suits
                    None
                }
            }
            (true, true) => match (&self.suit, &self.rank, &other.suit, &other.rank) {
                (Suit::Heart, Rank::Val10, Suit::Heart, Rank::Val10) => Some(Ordering::Equal),
                (Suit::Heart, Rank::Val10, _, _) => Some(Ordering::Greater),
                (_, _, Suit::Heart, Rank::Val10) => Some(Ordering::Less),
                (Suit::Club, Rank::Queen, Suit::Club, Rank::Queen) => Some(Ordering::Equal),
                (Suit::Club, Rank::Queen, _, _) => Some(Ordering::Greater),
                (_, _, Suit::Club, Rank::Queen) => Some(Ordering::Less),
                (Suit::Spade, Rank::Queen, Suit::Spade, Rank::Queen) => Some(Ordering::Equal),
                (Suit::Spade, Rank::Queen, _, _) => Some(Ordering::Greater),
                (_, _, Suit::Spade, Rank::Queen) => Some(Ordering::Less),
                (Suit::Heart, Rank::Queen, Suit::Heart, Rank::Queen) => Some(Ordering::Equal),
                (Suit::Heart, Rank::Queen, _, _) => Some(Ordering::Greater),
                (_, _, Suit::Heart, Rank::Queen) => Some(Ordering::Less),
                (Suit::Diamond, Rank::Queen, Suit::Diamond, Rank::Queen) => Some(Ordering::Equal),
                (Suit::Diamond, Rank::Queen, _, _) => Some(Ordering::Greater),
                (_, _, Suit::Diamond, Rank::Queen) => Some(Ordering::Less),
                (Suit::Club, Rank::Jack, Suit::Club, Rank::Jack) => Some(Ordering::Equal),
                (Suit::Club, Rank::Jack, _, _) => Some(Ordering::Greater),
                (_, _, Suit::Club, Rank::Jack) => Some(Ordering::Less),
                (Suit::Spade, Rank::Jack, Suit::Spade, Rank::Jack) => Some(Ordering::Equal),
                (Suit::Spade, Rank::Jack, _, _) => Some(Ordering::Greater),
                (_, _, Suit::Spade, Rank::Jack) => Some(Ordering::Less),
                (Suit::Heart, Rank::Jack, Suit::Heart, Rank::Jack) => Some(Ordering::Equal),
                (Suit::Heart, Rank::Jack, _, _) => Some(Ordering::Greater),
                (_, _, Suit::Heart, Rank::Jack) => Some(Ordering::Less),
                (Suit::Diamond, Rank::Jack, Suit::Diamond, Rank::Jack) => Some(Ordering::Equal),
                (Suit::Diamond, Rank::Jack, _, _) => Some(Ordering::Greater),
                (_, _, Suit::Diamond, Rank::Jack) => Some(Ordering::Less),
                (Suit::Diamond, Rank::Ace, Suit::Diamond, Rank::Ace) => Some(Ordering::Equal),
                (Suit::Diamond, Rank::Ace, _, _) => Some(Ordering::Greater),
                (_, _, Suit::Diamond, Rank::Ace) => Some(Ordering::Less),
                (Suit::Diamond, Rank::Val10, Suit::Diamond, Rank::Val10) => Some(Ordering::Equal),
                (Suit::Diamond, Rank::Val10, _, _) => Some(Ordering::Greater),
                (_, _, Suit::Diamond, Rank::Val10) => Some(Ordering::Less),
                (Suit::Diamond, Rank::King, Suit::Diamond, Rank::King) => Some(Ordering::Equal),
                (Suit::Diamond, Rank::King, _, _) => Some(Ordering::Greater),
                (_, _, Suit::Diamond, Rank::King) => Some(Ordering::Less),
                (_, _, _, _) => None, // illegal cases
            },
        }
    }
}

impl std::fmt::Display for Card {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let space = '\u{200A}';
        write!(f, "{}{}{}{}", self.suit(), space, self.rank(), space,)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_value() {
        for (rank, value) in &[
            (Rank::Ace, 11usize),
            (Rank::King, 4usize),
            (Rank::Queen, 3usize),
            (Rank::Jack, 2usize),
            (Rank::Val10, 10usize),
        ] {
            for suit in &[Suit::Club, Suit::Spade, Suit::Heart, Suit::Diamond] {
                assert_eq!(&Card::new(&suit, &rank).value(), value);
            }
        }
    }

    #[test]
    fn test_card_eq() {
        for suit_one in &[Suit::Club, Suit::Spade, Suit::Heart, Suit::Diamond] {
            for rank_one in &[Rank::Ace, Rank::King, Rank::Queen, Rank::Jack, Rank::Val10] {
                let card_one = Card::new(&suit_one, &rank_one);
                for suit_two in &[Suit::Club, Suit::Spade, Suit::Heart, Suit::Diamond] {
                    for rank_two in &[Rank::Ace, Rank::King, Rank::Queen, Rank::Jack, Rank::Val10] {
                        let card_two = Card::new(&suit_two, &rank_two);
                        if suit_one == suit_two && rank_one == rank_two {
                            assert_eq!(card_one, card_two);
                        } else {
                            assert_ne!(card_one, card_two);
                        }
                    }
                }
            }
        }
    }

    #[test]
    fn test_card_partialord_heart_10() {
        let card_one = Card::new(&Suit::Heart, &Rank::Val10);
        assert_eq!(
            card_one.partial_cmp(&card_one.clone()),
            Some(Ordering::Equal)
        );
        for r in &[Rank::Queen, Rank::Jack] {
            let card_two_club = Card::new(&Suit::Club, r);
            let card_two_spade = Card::new(&Suit::Spade, r);
            let card_two_heart = Card::new(&Suit::Heart, r);
            let card_two_diamond = Card::new(&Suit::Diamond, r);
            assert_eq!(
                card_one.partial_cmp(&card_two_club),
                Some(Ordering::Greater)
            );
            assert_eq!(
                card_one.partial_cmp(&card_two_spade),
                Some(Ordering::Greater)
            );
            assert_eq!(
                card_one.partial_cmp(&card_two_heart),
                Some(Ordering::Greater)
            );
            assert_eq!(
                card_one.partial_cmp(&card_two_diamond),
                Some(Ordering::Greater)
            );
            assert_eq!(card_two_club.partial_cmp(&card_one), Some(Ordering::Less));
            assert_eq!(card_two_spade.partial_cmp(&card_one), Some(Ordering::Less));
            assert_eq!(card_two_heart.partial_cmp(&card_one), Some(Ordering::Less));
            assert_eq!(
                card_two_diamond.partial_cmp(&card_one),
                Some(Ordering::Less)
            );
        }
        for r in &[Rank::Ace, Rank::Val10, Rank::King] {
            let card_two_diamond = Card::new(&Suit::Diamond, r);
            assert_eq!(
                card_one.partial_cmp(&card_two_diamond),
                Some(Ordering::Greater)
            );
            assert_eq!(
                card_two_diamond.partial_cmp(&card_one),
                Some(Ordering::Less)
            );
        }
        for s in &[Suit::Club, Suit::Spade, Suit::Heart] {
            for r in &[Rank::Ace, Rank::Val10, Rank::King] {
                if !(s == &Suit::Heart && r == &Rank::Val10) {
                    let card_two = Card::new(s, r);
                    assert_eq!(card_one.partial_cmp(&card_two), Some(Ordering::Greater));
                    assert_eq!(card_two.partial_cmp(&card_one), Some(Ordering::Less));
                }
            }
        }
    }

    #[test]
    fn test_card_is_trump() {
        let trump_cards = vec![
            Card {
                suit: Suit::Heart,
                rank: Rank::Val10,
            },
            Card {
                suit: Suit::Club,
                rank: Rank::Queen,
            },
            Card {
                suit: Suit::Spade,
                rank: Rank::Queen,
            },
            Card {
                suit: Suit::Heart,
                rank: Rank::Queen,
            },
            Card {
                suit: Suit::Diamond,
                rank: Rank::Queen,
            },
            Card {
                suit: Suit::Club,
                rank: Rank::Jack,
            },
            Card {
                suit: Suit::Spade,
                rank: Rank::Jack,
            },
            Card {
                suit: Suit::Heart,
                rank: Rank::Jack,
            },
            Card {
                suit: Suit::Diamond,
                rank: Rank::Jack,
            },
            Card {
                suit: Suit::Diamond,
                rank: Rank::Ace,
            },
            Card {
                suit: Suit::Diamond,
                rank: Rank::Val10,
            },
            Card {
                suit: Suit::Diamond,
                rank: Rank::King,
            },
        ];
        let nontrump_cards = vec![
            Card {
                suit: Suit::Club,
                rank: Rank::Ace,
            },
            Card {
                suit: Suit::Club,
                rank: Rank::Val10,
            },
            Card {
                suit: Suit::Club,
                rank: Rank::King,
            },
            Card {
                suit: Suit::Spade,
                rank: Rank::Ace,
            },
            Card {
                suit: Suit::Spade,
                rank: Rank::Val10,
            },
            Card {
                suit: Suit::Spade,
                rank: Rank::King,
            },
            Card {
                suit: Suit::Heart,
                rank: Rank::Ace,
            },
            Card {
                suit: Suit::Heart,
                rank: Rank::King,
            },
        ];

        for card in trump_cards.iter() {
            assert!(card.is_trump(), "card {} should be trump", card);
        }

        for card in nontrump_cards.iter() {
            assert!(!card.is_trump(), "card {} should not be trump", card);
        }

        // check if all cards are listed
        let cards = [trump_cards, nontrump_cards].concat();
        for s in &[Suit::Club, Suit::Spade, Suit::Heart] {
            for r in &[Rank::Ace, Rank::Val10, Rank::King] {
                let card = Card::new(s, r);
                assert!(cards.contains(&card), "missing card {} detected", card);
            }
        }
    }

    #[test]
    fn test_card_is_following() {
        let trump_cards = vec![
            Card {
                suit: Suit::Heart,
                rank: Rank::Val10,
            },
            Card {
                suit: Suit::Club,
                rank: Rank::Queen,
            },
            Card {
                suit: Suit::Spade,
                rank: Rank::Queen,
            },
            Card {
                suit: Suit::Heart,
                rank: Rank::Queen,
            },
            Card {
                suit: Suit::Diamond,
                rank: Rank::Queen,
            },
            Card {
                suit: Suit::Club,
                rank: Rank::Jack,
            },
            Card {
                suit: Suit::Spade,
                rank: Rank::Jack,
            },
            Card {
                suit: Suit::Heart,
                rank: Rank::Jack,
            },
            Card {
                suit: Suit::Diamond,
                rank: Rank::Jack,
            },
            Card {
                suit: Suit::Diamond,
                rank: Rank::Ace,
            },
            Card {
                suit: Suit::Diamond,
                rank: Rank::Val10,
            },
            Card {
                suit: Suit::Diamond,
                rank: Rank::King,
            },
        ];
        let club_cards = vec![
            Card {
                suit: Suit::Club,
                rank: Rank::Ace,
            },
            Card {
                suit: Suit::Club,
                rank: Rank::Val10,
            },
            Card {
                suit: Suit::Club,
                rank: Rank::King,
            },
        ];
        let spade_cards = vec![
            Card {
                suit: Suit::Spade,
                rank: Rank::Ace,
            },
            Card {
                suit: Suit::Spade,
                rank: Rank::Val10,
            },
            Card {
                suit: Suit::Spade,
                rank: Rank::King,
            },
        ];
        let heart_cards = vec![
            Card {
                suit: Suit::Heart,
                rank: Rank::Ace,
            },
            Card {
                suit: Suit::Heart,
                rank: Rank::King,
            },
        ];

        // following cases

        for first_card in trump_cards.iter() {
            for second_card in trump_cards.iter() {
                assert!(
                    second_card.following(first_card),
                    "card {} should follow {}",
                    second_card,
                    first_card
                );
            }
        }

        for first_card in club_cards.iter() {
            for second_card in club_cards.iter() {
                assert!(
                    second_card.following(first_card),
                    "card {} should follow {}",
                    second_card,
                    first_card
                );
            }
        }

        for first_card in spade_cards.iter() {
            for second_card in spade_cards.iter() {
                assert!(
                    second_card.following(first_card),
                    "card {} should follow {}",
                    second_card,
                    first_card
                );
            }
        }

        for first_card in heart_cards.iter() {
            for second_card in heart_cards.iter() {
                assert!(
                    second_card.following(first_card),
                    "card {} should follow {}",
                    second_card,
                    first_card
                );
            }
        }

        // non-following cases

        // trump
        for first_card in trump_cards.iter() {
            for second_card in club_cards.iter() {
                assert!(
                    !second_card.following(first_card),
                    "card {} should not follow {}",
                    second_card,
                    first_card
                );
            }
        }
        for first_card in trump_cards.iter() {
            for second_card in spade_cards.iter() {
                assert!(
                    !second_card.following(first_card),
                    "card {} should not follow {}",
                    second_card,
                    first_card
                );
            }
        }
        for first_card in trump_cards.iter() {
            for second_card in heart_cards.iter() {
                assert!(
                    !second_card.following(first_card),
                    "card {} should not follow {}",
                    second_card,
                    first_card
                );
            }
        }

        // club
        for first_card in club_cards.iter() {
            for second_card in trump_cards.iter() {
                assert!(
                    !second_card.following(first_card),
                    "card {} should not follow {}",
                    second_card,
                    first_card
                );
            }
        }
        for first_card in club_cards.iter() {
            for second_card in spade_cards.iter() {
                assert!(
                    !second_card.following(first_card),
                    "card {} should not follow {}",
                    second_card,
                    first_card
                );
            }
        }
        for first_card in club_cards.iter() {
            for second_card in heart_cards.iter() {
                assert!(
                    !second_card.following(first_card),
                    "card {} should not follow {}",
                    second_card,
                    first_card
                );
            }
        }

        // spade
        for first_card in spade_cards.iter() {
            for second_card in trump_cards.iter() {
                assert!(
                    !second_card.following(first_card),
                    "card {} should not follow {}",
                    second_card,
                    first_card
                );
            }
        }
        for first_card in spade_cards.iter() {
            for second_card in club_cards.iter() {
                assert!(
                    !second_card.following(first_card),
                    "card {} should not follow {}",
                    second_card,
                    first_card
                );
            }
        }
        for first_card in spade_cards.iter() {
            for second_card in heart_cards.iter() {
                assert!(
                    !second_card.following(first_card),
                    "card {} should not follow {}",
                    second_card,
                    first_card
                );
            }
        }

        // heart
        for first_card in heart_cards.iter() {
            for second_card in club_cards.iter() {
                assert!(
                    !second_card.following(first_card),
                    "card {} should not follow {}",
                    second_card,
                    first_card
                );
            }
        }
        for first_card in heart_cards.iter() {
            for second_card in trump_cards.iter() {
                assert!(
                    !second_card.following(first_card),
                    "card {} should not follow {}",
                    second_card,
                    first_card
                );
            }
        }
        for first_card in heart_cards.iter() {
            for second_card in spade_cards.iter() {
                assert!(
                    !second_card.following(first_card),
                    "card {} should not follow {}",
                    second_card,
                    first_card
                );
            }
        }

        // check if all cards are listed
        let cards = [trump_cards, club_cards, spade_cards, heart_cards].concat();
        for s in &[Suit::Club, Suit::Spade, Suit::Heart] {
            for r in &[Rank::Ace, Rank::Val10, Rank::King] {
                let card = Card::new(s, r);
                assert!(cards.contains(&card), "missing card {} detected", card);
            }
        }
    }
}
