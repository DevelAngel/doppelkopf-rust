use crate::card::Card;

#[derive(Clone)]
pub struct HiddenCard {
    card: Option<Card>,
}

impl HiddenCard {
    pub fn hidden() -> Self {
        Self { card: None }
    }

    pub fn shown(card: Card) -> Self {
        Self { card: Some(card) }
    }

    pub fn card(&self) -> &Option<Card> {
        &self.card
    }
}
