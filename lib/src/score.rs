use crate::card::Card;
use crate::gamecard::GameCard;
use crate::rank::Rank;

pub(crate) trait Score {
    fn score(&self) -> usize;
}

impl Score for Card {
    fn score(&self) -> usize {
        match self.rank() {
            Rank::Ace => 11,
            Rank::King => 4,
            Rank::Queen => 3,
            Rank::Jack => 2,
            Rank::Val10 => 10,
        }
    }
}

impl Score for GameCard {
    fn score(&self) -> usize {
        self.card().score()
    }
}

impl Score for [Card] {
    fn score(&self) -> usize {
        let mut score = 0;
        for c in self.iter() {
            score += c.score();
        }
        score
    }
}

impl Score for [GameCard] {
    fn score(&self) -> usize {
        let mut score = 0;
        for c in self.iter() {
            score += c.card().score();
        }
        score
    }
}

impl Score for Vec<GameCard> {
    fn score(&self) -> usize {
        self.as_slice().score()
    }
}
