use crate::deck::{Deck, DeckCards};
use crate::player::Player;

pub struct Dealer {
    deck: DeckCards,
}

impl Dealer {
    pub fn new() -> Self {
        let mut cards = DeckCards::init_cards();
        cards.shuffle_cards();
        Self { deck: cards }
    }

    pub fn deal_out_three_cards(&mut self, players: &mut [Box<dyn Player>; 4]) {
        for p in players.iter_mut() {
            let cards = self.deck.draw::<3>().expect("not enough cards");
            p.take_three_cards(cards);
        }
    }

    pub fn deal_out_four_cards(&mut self, players: &mut [Box<dyn Player>; 4]) {
        for p in players.iter_mut() {
            let cards = self.deck.draw::<4>().expect("not enough cards");
            p.take_four_cards(cards);
        }
    }
}
