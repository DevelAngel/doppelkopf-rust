use crate::gamecard::GameCard;
use crate::rank::Rank;
use crate::suit::Suit;

use rand::seq::SliceRandom; // shuffle method

use std::mem::{self, MaybeUninit};

pub(crate) type DeckCards = Vec<GameCard>;

pub(crate) trait Deck {
    fn init_cards() -> Self;
    fn shuffle_cards(&mut self);
    fn draw<const N: usize>(&mut self) -> Option<[GameCard; N]>;
}

impl Deck for DeckCards {
    fn init_cards() -> Self {
        let size = 4 * 5 * 2;
        let mut cards = Vec::with_capacity(size);
        for suit in &[Suit::Club, Suit::Spade, Suit::Heart, Suit::Diamond] {
            for rank in &[Rank::Ace, Rank::King, Rank::Queen, Rank::Jack, Rank::Val10] {
                cards.push(GameCard::new(suit, rank));
                cards.push(GameCard::new(suit, rank));
            }
        }
        assert_eq!(size, cards.len());
        cards
    }

    fn shuffle_cards(&mut self) {
        self.shuffle(&mut rand::thread_rng());
    }

    fn draw<const N: usize>(&mut self) -> Option<[GameCard; N]> {
        if self.len() >= N {
            let mut array: [MaybeUninit<GameCard>; N] =
                unsafe { MaybeUninit::uninit().assume_init() };
            for elem in &mut array[..] {
                elem.write(self.pop().unwrap());
            }
            //let array = unsafe { MaybeUninit::array_assume_init(array) }; // nightly feature
            let array = unsafe { mem::transmute_copy(&array) };
            Some(array)
        } else {
            assert_eq!(self.len(), 0);
            None
        }
    }
}
