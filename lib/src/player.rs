mod ki;
mod tui;

use crate::announcement::VorbehaltAnnouncement;
use crate::gamecard::GameCard;
use crate::hiddencard::HiddenCard;

use uuid::UuidBytes;

pub trait Player {
    fn is_interactive(&self) -> bool;
    fn uuid(&self) -> &UuidBytes;
    fn name(&self) -> &str;
    fn hand(&self) -> Vec<HiddenCard>;
    fn hand_len(&self) -> usize;
    fn take_three_cards(&mut self, cards: [GameCard; 3]);
    fn take_four_cards(&mut self, cards: [GameCard; 4]);
    fn announce_vorbehalt(&self) -> VorbehaltAnnouncement;
    fn play_card(
        &mut self,
        played: &[GameCard],
        f: &mut dyn FnMut(&[usize]) -> Option<usize>,
    ) -> GameCard;
}

pub trait GamePlayer: Player {
    fn new(name: &str, ouvert: bool) -> Self
    where
        Self: Sized;
}

impl Eq for dyn Player {} // a == a
impl PartialEq for dyn Player {
    fn eq(&self, other: &Self) -> bool {
        self.uuid() == other.uuid()
    }
}

// re-export
pub use self::ki::KIPlayer;
pub use self::tui::TUIPlayer;
