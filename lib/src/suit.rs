#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Suit {
    Club,
    Spade,
    Heart,
    Diamond,
}

impl std::fmt::Display for Suit {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match &self {
            Suit::Club => write!(f, "\u{2663}"),
            Suit::Spade => write!(f, "\u{2664}"),
            Suit::Heart => write!(f, "\u{2665}"),
            Suit::Diamond => write!(f, "\u{2666}"),
        }
    }
}
